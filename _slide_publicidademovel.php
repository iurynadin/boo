<div class="holderItems">
	<div class="sliderTitle">Engajamento móvel e inteligência de localização</div>
	<div class="sliderSubtitle">Melhore a relevância dos anúncios digitais e  o valor do CPM com localização e proximidade</div>

	<div class="holderMainIcon hidden-xs">
		<img src="dist/imgs/publishers/pub_secundario.svg" width="102" alt="">
	</div>

	<div class="holderSecondIcon hidden-xs">
		<img src="dist/imgs/slider/icon/icon_pub2_secundario.svg" width="97" alt="">
	</div>
</div>