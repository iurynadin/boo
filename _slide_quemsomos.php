<div class="holderItems">
	<div class="sliderTitle">Engajamento móvel via proximidade</div>
	<div class="sliderSubtitle">Use experiências no celular para capturar momentos de significância.</div>

	<div class="holderMainIcon hidden-xs">
		<img src="dist/imgs/slider/icon/icon_home.svg" width="130" alt="">
	</div>

	<div class="holderSecondIcon hidden-xs">
		<img src="dist/imgs/slider/icon/icon_app_secundario.svg" width="132" alt="">
	</div>
</div>