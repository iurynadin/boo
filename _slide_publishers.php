<div class="holderItems">
	<div class="sliderTitle">Engajamento móvel e inteligência de localização</div>
	<div class="sliderSubtitle">Crie experiências envolventes e consistentes em aplicativos móveis.</div>

	<div class="holderMainIcon hidden-xs">
		<img src="dist/imgs/slider/icon/icon_publicidademovel.svg" width="80" alt="">
	</div>

	<div class="holderSecondIcon hidden-xs">
		<img src="dist/imgs/publishers/pub_secundario.svg" width="97" alt="">
	</div>

</div>