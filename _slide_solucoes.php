<div class="holderItems">
		<div class="sliderTitle">Maximize sua estratégia móvel</div>
		<div class="sliderSubtitle">Criação, gerenciamento e análise de campanhas para celular e infra-estrutura de localização</div>
		<div class="container contSlider hidden-xs">
			<div class="row">
				<div class="col-xs-6 col-md-3 iconLocalizacao">
					<span>Inteligência de Localização</span>
				</div>
				<div class="col-xs-6 col-md-3 iconProximidade">
					<span>Geofacing</span>
				</div>
				<div class="col-xs-6 col-md-3 iconAnalytics">
					<span>Console de Gerenciamento</span>
				</div>
				<div class="col-xs-6 col-md-3 iconDatasolutions">
					<span>MONETIZACAO DE DADOS</span>
				</div>
			</div>
		</div>
	</div>