<div class="holderItems">
	<div class="sliderTitle">Plataforma de dados de localização móvel</div>
	<div class="sliderSubtitle">Criação, gerenciamento e análise de campanhas para celular e infra-estrutura de localização</div>

	<div class="holderMainIcon hidden-xs">
		<img src="dist/imgs/slider/icon/icon_sdk.svg" width="90" alt="">
	</div>

	<div class="holderSecondIcon hidden-xs">
		<img src="dist/imgs/slider/icon/icon_sdk_apoio.svg" width="230" class="hidden-sm" alt="">
		<img src="dist/imgs/slider/icon/icon_sdk_apoio.svg" width="180" class="visible-sm-block" alt="">
	</div>

	<p class="hidden-xs hidden-sm">Entender a jornada do cliente antes, durante e após visitarem seus locais físicos determinados.</p>
</div>