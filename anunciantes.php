<?php 
$titulo = 'Anunciantes';
include '_meta.php';
?>

</head>

<body class="anunciantes">

	<?php include '_header.php'; ?>
	
	<div class="slider">
		
		<div id="headerCarrousel" class="carousel slide" data-interval="false" data-wrap="true" data-ride="carousel" data-keyboard="true">

			<div class="carousel-inner" role="listbox">

				<div class="item item1-publisher" id="0">
					<a href="publishers.php">
						<?php include '_slide_publishers.php'; ?>
					</a>
				</div>

				<div class="item item1-anunciantes active" id="1">
					<?php include '_slide_anunciantes.php'; ?>
				</div>

				<div class="item item1" id="2">
					<a href="solucoes.php">
						<?php include "_slide_solucoes.php"; ?>
					</a>
				</div>
			</div>
			<?php include '_slide_controls.php'; ?>
		</div>

		<a href="#publishers" class="sliderDown">down</a>

	</div> 
	<!-- slider -->


	<section class="faixaPercent">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-lg-2 text-center counter count01">
					60%
				</div>

				<div class="col-md-8 col-lg-8 text-center texto">
					<h4>CAMPANHAS HABILITADAS PARA BEACONS TEM UMA TAXA MÉDIA DE CLIQUES DE 60%<br>
					<span>EM COMPARAÇÃO COM AS CAMPANHAS TRADICIONAIS COMO EMAIL MARKETING DE 2%</span></h4>
				</div>

				<div class="col-md-2 col-lg-2">
					<img src="dist/imgs/anunciantes/icon-faixa01.svg" class="image-center iconcounter01" alt="">
				</div>
			</div>
		</div>
	</section>

	<section class="publishers" id="publishers">
		<div class="container">
			<div class="row">	
				<div class="col-xs-12">
					<img src="dist/imgs/anunciantes/anunciantes01.svg" width="240" class="img-responsive anunc01 hidden-md hidden-lg" alt="">
					<h2>Gimbal para marcas e anunciantes</h2>
				</div>

				<div class="col-xs-12 col-md-7">
					
					<p class="centerTablet">

					Use a localização para maximizar o ROI em campanhas móveis <br><br>

					Conecte-se com os consumidores quando e onde realmente importa através de ricas experiências, tanto em contexto como também em tempo real. <br><br>

					Nós capacitamos as marcas para envolverem-se com seu público-alvo de uma maneira anteriormente impossível. Utilizando nossa rede de aplicativos de localização e de proximidade, as marcas podem não somente implantar campanhas inovadoras orientadas para maximizar o ROI diretamente para um público-alvo em pontos específicos de interesse, mas também podem oferecer conteúdo e ofertas personalizadas para esse público. 
					</p>
				</div>
				<div class="col-xs-12 col-md-5 hidden-xs hidden-sm">
					<img src="dist/imgs/anunciantes/anunciantes01.svg" width="240" class="img-responsive anunc01" alt="">
				</div>
			</div>

		</div>

	</section>



	<section class="others bgCinza">

		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="text-center">Você pode usar o Gimbal para:</h3>
				</div>
			</div>

			<div class="row rowLocalizacaoAnunc">

				<div class="col-md-6">
					<img src="dist/imgs/negocios/icon_localizacao01.svg" width="270" class="iconAnunOthers" alt="">
				</div>

				<div class="col-md-6">
					<ul class="feature feat3">
						<li>AUMENTE A PERCEPÇÃO DE SEUS LOCAIS OU PRODUTOS MAIS PRÓXIMOS</li>
						<li>ENTREGE CUPONS OU OFERTAS A DISPOSITIVOS MÓVEIS EM PONTOS DE INTERESSE </li>
						<li>FORNECA ATRIBUIÇÕES EM AMBIENTES FÍSICOS</li>
						<li>AUMENTE O FLUXO DE TRANSEUNTES PARA SEUS LOCAIS/NEGÓCIOS </li>
						<li>INTERAJA COM SEUS PÚBLICO DESEJADO DE FORMA PERSONALIZADA E SIGNIFICATIVA</li>
					</ul>
				</div>

			</div>

		</div>

	</section>



	<section class="faixaPercent">
		<div class="container">
			<div class="row">
				<div class="col-md-2 text-center counter count01">21%</div>

				<div class="col-md-9 text-center">
					<h4>OS PROGRAMAS QUE CONECTAM ANUNCIOS OOH COM UMA CAMPANHA  <br>
					<span>TIVERAM UMA TAXA DE ABERTURA DE 21 % MOVEL COM UMA TAXA DE CONVERSAO DE 12% VISITANDO UMA LOJA</span></h4>
					<p class="white">* Taxa de conversão de uma típica campanha móvel é de 1% ou menos. </p>
				</div>
			</div>
		</div>
	</section>



	<section class="outofhome" id="outofhome">
		<div class="container">
			<div class="row">	
				<div class="col-xs-12">
					<img src="dist/imgs/anunciantes/anunciantes02.svg" width="150" class="img-responsive anunc02 hidden-md hidden-lg" alt="">
					<h2>GIMBAL PARA “OUT-OF-HOME” (OOH)</h2>
				</div>

				<div class="col-xs-12 col-md-7">
					
					<p class="centerTablet">
					Conecte-se com audiências móveis no mundo físico e, finalmente, forneça uma verdadeira atribuição as campanhas. </p>

					<h4 class="centerTablet">OOH NETWORKS</h4>

					<p class="centerTablet">Conecte-se com os consumidores quando e onde realmente importa através de ricas experiências, tanto em contexto como também em tempo real. <br><br>

					Nós capacitamos as marcas para envolverem-se com seu público-alvo de uma maneira anteriormente impossível. Utilizando nossa rede de aplicativos de localização e de proximidade, as marcas podem não somente implantar campanhas inovadoras orientadas para maximizar o ROI diretamente para um público-alvo em pontos específicos de interesse, mas também podem oferecer conteúdo e ofertas personalizadas para esse público. 
					</p>
				</div>
				<div class="col-xs-12 col-md-5 hidden-xs hidden-sm">
					<img src="dist/imgs/anunciantes/anunciantes02.svg" width="150" class="img-responsive anunc02" alt="">
				</div>
			</div>

		</div>

	</section>



	<section class="oohFaixa" data-parallax="scroll" data-image-src="dist/imgs/backgrounds/parallax_anuciantes.jpg">
		<div class="container">
			<div class="row">
				
				<div class="col-md-offset-1 col-md-10">

					<div class="row">
						<div class="col-xs-12">						
							<h4 class="text-center">A Plataforma Gimbal pode ajudá-lo: </h4>
							<p>
							Aumentar os esforços de “shopper marketing”, por ter uma melhor compreensão da jornada de um consumidor. Incremente as vendas na loja enviando conteúdo móvel relevante com base em proximidade para seus ativos habilitados para “beacons”
							</p> 
							
						</div>
					</div>

					<div class="row">

						<div class="col-xs-offset-3 col-xs-3">
							<img src="dist/imgs/negocios/icon_financ02.svg" class="img-responsive image-center" width="200" alt="">
						</div>

						<div class="col-xs-3">
							<img src="dist/imgs/anunciantes/icon0cart.svg" class="img-responsive image-center icon-cart" width="110" alt="">
						</div>

						<div class="col-xs-12">	
							<br>					
							<p>
							Prove os benefícios de OOH sendo incorporados em campanhas de mídia.
							</p> 
							
						</div>

					</div>
				</div>
			</div>

		</div>
	</section>



	<?php include '_footer.php'; ?>
	
<script src="dist/js/parallax.min.js"></script>

</body>

</html>