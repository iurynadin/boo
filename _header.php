<!-- <div class="breakpoint"><p></p></div> -->
<header>
	<nav class="navbar navbar-default navbar-fixed-top" id="mainNav">
		
		<div class="container">

			<div class="container-fluid">

				<div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                        <div class="logo"></div>
                    </a>
                </div>

                	<ul class="nav navbar-nav navbar-right visible-lg-block">
						
						<li class="logogimbal">
							<img src="dist/imgs/ui/logo_gimbal.svg" width="120" alt="">
						</li>

                        <li class="dropdown">
                            <a href="solucoes.php" class="dropdown-toggle"  role="button" aria-haspopup="true" aria-expanded="true">
                                Soluções
                            </a> <!-- removido o data-toggle="dropdown" -->
                            <ul class="dropdown-menu">
                                <li><a href="#plataforma" data-page="solucoes">Plataforma Gimbal</a></li>
                                <li><a href="#geofences" data-page="solucoes">Geofences de Última Geração</a></li>
                                <li><a href="#beacons" data-page="solucoes">Beacons de micro-localização</a></li>
                                <li><a href="#experiencias" data-page="solucoes">Experiências Gimbal</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="negocios.php" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="true">
                                Negócios
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#varejo" data-page="negocios">Gimbal para o varejo</a></li>
                            	<li><a href="#hospitalidade" data-page="negocios">Gimbal para hospitalidade</a></li>
                            	<li><a href="#eventos" data-page="negocios">Gimbal para espaço de evento</a></li>
                            	<li><a href="#financeiros" data-page="negocios">Gimbal para serviços finaceiros</a></li>
                            </ul>
                        </li>

                        <li><a href="publishers.php">Publishers</a></li>

                        <li class="dropdown">
                            <a href="anunciantes.php" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="true">Anúnciantes</a>
                            <ul class="dropdown-menu">
                                <li><a href="#publishers" data-page="anunciantes">Gimbal para marcas e anunciantes</a></li>
                            	<li><a href="#outofhome" data-page="anunciantes"> Gimbal para out of home networks</a></li>
                            </ul>
                        </li>

                        <li><a href="http://www.gimbal.com/developers/" target="blank">Developers</a></li>
                    </ul>

                <!-- </div>  -->
                <!-- id="navbar" -->



			</div><!--/.container-fluid -->
		</div>

		<div class="mbr-navbar__hamburger mbr-hamburger text-white">
			<span class="mbr-hamburger__line"></span>
		</div>
	</nav>
	<!-- <div class="logo"></div> -->
</header>


<div class="menuSecundario none hidden-lg">

	<div class="container">
		<div class="row">
			
			<div class="col-md-6 col-md-offset-3">
				
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						
						<div class="panel panel-default">

							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" class="btnBoo" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Boo!</a>
								</h4>
							</div>

							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<ul id="secundario">
								        <li><a href="index.php" class="boo">Quem Somos</a></li>
								        <li><a href="sdk.php" class="boo">Nosso Software</a></li>
								        <li><a href="app.php" class="boo">Nosso Aplicativo</a></li>
								        <li><a href="publicidademovel.php" class="boo">Publicidade Móvel</a></li>
								        <li><a href="blog.php" class="boo">Blog</a></li>
								    </ul>
								</div>
							</div>

					</div>


						<div class="panel panel-default">

							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed btnGimbal" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Gimbal<span>Technologies</span></a>
								</h4>
							</div>

							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									<ul id="secundarioGimbal">
										<li><a href="solucoes.php" class="gimbal">Soluções</a></li>
								        <li><a href="negocios.php" class="gimbal">Negócios</a></li>
								        <li><a href="publishers.php" class="gimbal">Publishers</a></li>
								        <li><a href="anunciantes.php" class="gimbal">Anúnciantes</a></li>
								        <li><a href="http://www.gimbal.com/developers/" target="_blank" class="gimbal">Developers</a></li>
									</ul>
								</div>
							</div>

						</div>

					</div>
					<!-- panel-group -->

			</div>

		</div>
	</div>
	
</div>

</div>

