<?php 
$titulo = 'Home';
include '_meta.php';
?>
<style>
	.item{
        overflow: hidden;
        background-repeat: no-repeat;
        background-position: center center;
        background-color: #eee;
        height: auto;
    }
</style>
</head>

<body>

	<?php include '_header.php'; ?>

	<div class="slider">

	<!-- <img src="dist/imgs/ui/slider/btn-right.svg" alt=""> -->
	
	<div id="headerCarrousel" class="carousel slide" data-interval="false" data-wrap="true" data-ride="carousel" data-keyboard="true">

		<div class="carousel-inner" role="listbox">

			<div class="item active" id="0" style="overflow: hidden;
        background-repeat: no-repeat;
        background-position: center center;
        background-color: #eee;
        height: auto;">
				<img src="dist/imgs/slider/app.jpg" alt="" style="width:100%;">
			</div>

			<!-- <div class="item" id="0">
				<img src="dist/imgs/slider/app.jpg" height="1150" width="2000" alt="">
			</div>
			 -->
			<!-- <div class="item item2" id="1">
				<div class="holderItems">
					<div class="sliderTitle">Título</div>
					<p>Subtitulo</p>
				</div>
			</div> -->

		</div>


		<ol class="carousel-indicators">
			<li data-target="#headerCarrousel" data-slide-to="0" class="active"></li>
			<li data-target="#headerCarrousel" data-slide-to="1"></li>
			<!-- <li data-target="#headerCarrousel" data-slide-to="2"></li>
			<li data-target="#headerCarrousel" data-slide-to="3"></li> -->
		</ol>


		<a class="left carousel-control hidden-xs" href="#headerCarrousel" role="button" data-slide="prev"></a>
		<a class="right carousel-control hidden-xs" href="#headerCarrousel" role="button" data-slide="next"></a>

	</div>

</div>


	<!-- <div class="video">
		<iframe width="1060" height="596" src="https://www.youtube.com/embed/ebQm1X_lEbk?feature=oembed&rel=0&amp;showinfo=0" frameborder="0" allowfullscreen=""></iframe>
	</div> -->


	<?php include "_menuinterna_boo.php"; ?>
	


	<div class="container">
		<br>
		<h2>Blog</h2>

		<article class="post">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<span class="data">22.12.2016</span>
					<a href="#" class="postTitle">CLEVELAND CAVALLIERS INVOVA COM A GIMBAL</a>
					<div class="publisher">Publicado por <a href="#"><i>Vinícius</i></a></div>

					<a href="#">
						<img src="dist/imgs/blog/postage1_1.jpg" class="postImg img-responsive" alt="">
					</a>

					<div class="blockquotes">
						Criar a melhor experiência para os fãs durante um jogo do Cavaliers é uma das nossas principais prioridades. Ao dar aos fãs a opção de receberem notificações push através do aplicativo do Cavaliers, possibilitamos que eles desfrutem de um serviço personalizado, uma experiência interativa no The Q (Arena do time) que eleva o relacionamento entre a equipe e os nossos fãs à um um patamar nunca antes visto.
						<span class="author">Michael Conley,</span>
						<span class="subauthor">Vice-presidente de Serviços Digitais e Web do Cleaveland Cavaliers</span>
					</div>

					<h3>Situação</h3>

					<p>Com alguns dos fãs mais fanáticos da NBA, o Cleveland Cavaliers têm investido pesado em sua experiência com seus torcedores em sua arena. <br> <br>
					Limitações em seu aplicativo móvel não permitiam dar aos fãs a melhor experiência possível, conectando  o lado de dentro e de fora do popular Quicken Loans Arena (The Q), como também  proporcionavam oportunidades de engajamento muito limitadas</p>

					<br>
					<img src="dist/imgs/blog/postagem1_2.jpg" width="310" class="img-responsive" alt="">
					<br>

					<h3>Solução</h3>

					<p>YinzCam e Gimbal formaram uma parceria  para criar uma nova e aprimorada versão do App do Cleveland Cavaliers para a temporada de 2014 -  2015 da NBA. <br><br> 
					Beacons foram instalados em toda a arena e em locais externos. <br><br> 
					Mensagens de push baseadas em proximidade e localização oferecem conteúdo exclusivo e relevante diretamente aos fãs, incluindo mensagens do presindente do time, a escalação do time e fatos divertidos, como celebridades presentes na platéia  ou a proximidade ao mascote da equipe. <br><br> 
					O gerenciador da Gimbal torna mais fácil atualizar os beacons mesmo quando eles estão fora de alcance, atualizar o conteúdo de mensagens e fornecer gerenciamento dos insights analíticos para entender o sucesso da solução e comparar com ações anteriores, como também para aprofundar o engajamento com os usuários.</p>

					<br>
					<img src="dist/imgs/blog/postagem1_3.jpg" width="347" alt="">
					<br>

					<h3>Resultado</h3>
					<p>Mais de 82.000 visitas  aos beacons  e mais de 138.000 check-ins. Vendas e o engajamento com os fãs aumentaram significantemente para os Cavaliers e seus parceiros  na arena e em eventos fora do local. <br><br>
					Os Cavaliers agora irão avaliar os dados de atribuição e do caminho até a conversão, quando uma oferta/promoção é iniciada e quão rápido ela é usada. Os varejistas fora da arena têm a oportunidade de conectar com  o interesse dos fãs de esportes oferendo ofertas de varejo, graças à localização Gimbal habilitando a funcionalidade baseada em proximidade e localização. <br><br>
					Com a Gimbal, os Cavaliers têm a oportunidade de entregar possibilidades de engajamentos mais personalizados e relevantes e explorar uma personalização mais profunda para cada fã.</p>
					<hr>
					<div class="holderCateg">
						<a href="#" class="categoria">Case</a>
						<a href="#" class="categoria">Esporte</a>						
					</div>
					
				</div>
			</div>
		</article>




		<article class="post">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<span class="data">12.01.2017</span>
					<a href="#" class="postTitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas repudiandae iusto maxime laboriosam deserunt.</a>
					<div class="publisher">Publicado por <a href="#"><i>Charles Darwin</i></a></div>
					<a href="#">
						<img src="dist/imgs/blog/postagem2_1.gif" class="postImg postImgMt img-responsive" alt="">
					</a>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem asperiores magnam veniam officiis, accusantium quisquam ducimus dolorem placeat dolores. Eius ab necessitatibus maxime voluptatibus repellat non incidunt totam porro doloribus ex, provident fugiat id facere corrupti veniam, inventore deleniti tenetur quo reiciendis. In repellat rerum ratione nihil quas fugiat, facilis autem magnam ipsam odit ab, quasi veniam reprehenderit impedit quae atque inventore esse. <br><br>
					Cupiditate eius, reiciendis, rem labore consequuntur perspiciatis adipisci, totam ipsam debitis ex delectus laborum sapiente deleniti. Odit fugit nesciunt reprehenderit a aliquid itaque laborum eaque beatae non ea tempora sequi cum quis voluptates eos minus facilis, sint recusandae illum, qui suscipit perspiciatis libero enim iusto! Ad amet, id minus natus velit corrupti in suscipit rem placeat inventore voluptas, libero dignissimos, quibusdam modi laborum et temporibus blanditiis non expedita dolorum asperiores aspernatur? Fugit reiciendis porro aperiam, nostrum modi vel voluptatibus earum neque.</p>

					<hr>

					<div class="holderCateg">
						<a href="#" class="categoria">Tech</a>
						<a href="#" class="categoria">Evento</a>						
					</div>
					
					
				</div>
			</div>
		</article>
		
	</div>





	<section class="team">

		<div class="experienciasIcons" data-parallax="scroll" data-image-src="dist/imgs/backgrounds/parallax_team.jpg" > 

			<div class="container">

				<div class="row">
					<div class="col-xs-12">
						<h2>Time</h2>
					</div>
				</div>
				
				<div class="row rowTeam">
					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Scott Meadow</h5>
						<div class="cargo">CEO</div>
						<p>Shopper Marketing, comunicação interativa, tecnologia da informação e gerenciamento de projetos.</p>
						<a href="mailto:scott.meadow@boo.social" class="emailOnPurple">scott.meadow@boo.social</a>
					</div>

					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Vinícius Neves</h5>
						<div class="cargo">COO</div>
						<p>Desenvolvimento de novos negócios, produção, planejamento e gerenciamento de projetos.</p>
						<a href="mailto:vinicius.neves@boo.social" class="emailOnPurple">vinicius.neves@boo.social</a>
					</div>


					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Eduardo Morelli</h5>
						<div class="cargo">CFO</div>
						<p>Expert em gestão financeira.</p>
						<a href="mailto:eduardo.morelli@boo.social" class="emailOnPurple">eduardo.morelli@boo.social</a> 
					</div>

					<div class="clearfix visible-sm-block visible-md-block visible-lg-block"></div>

					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Guilherme Franco</h5>
						<div class="cargo">DIRETOR DE NOVOS NEGOCIOS</div>
						<p>Vendas,estratégia de Midia digital e novos negócios.</p>
						<a href="mailto:guilherme.franco@boo.social" class="emailOnPurple">guilherme.franco@boo.social</a>
					</div>

					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Renato Salles</h5>
						<!-- <div class="cargo">COO</div> -->
						<p>Expert em Midia OOH, Desenvolvimento de Negócios & Investidor Anjo.</p> 
						<a href="mailto:renato.salles@boo.social" class="emailOnPurple">renato.salles@boo.social</a>
					</div>

					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Rubens Nigro</h5>
						<!-- <div class="cargo">CFO</div> -->
						<p>Expert em Midia OOH, Desenvolvimento de Negócios & Investidor Anjo.</p> 
						<a href="mailto:rubens.nigro@boo.social" class="emailOnPurple">rubens.nigro@boo.social</a>
					</div>

				</div>
			</div>
		</div>
	</section>


	<?php include '_footer.php'; ?>
	

<script src="dist/js/parallax.min.js"></script>

</body>

</html>