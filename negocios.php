<?php 
$titulo = 'Negócios';
include '_meta.php';
?>

</head>

<body>

	<?php include '_header.php'; ?>

	
	<div class="slider negocios">
		
		<div id="headerCarrousel" class="carousel slide" data-interval="false" data-wrap="true" data-ride="carousel" data-keyboard="true">

			<div class="carousel-inner" role="listbox">

				<div class="item item1" id="0">
					<a href="solucoes.php">
						<?php include "_slide_solucoes.php"; ?>
					</a>
				</div>

				<div class="item item1-negocios active" id="1">
					<?php include '_slide_negocios.php'; ?>
				</div>
				
				<div class="item item1-publisher" id="2">
					<a href="publishers.php">
						<?php include '_slide_publishers.php'; ?>
					</a>
				</div>

			</div>

			<?php include '_slide_controls.php'; ?>

		</div>

		<a href="#varejo" class="sliderDown">down</a>

	</div> 
	<!-- slider -->


	<section class="varejo" id="varejo" >
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2>Gimbal para o varejo</h2>
					<h4 class="centerTablet">Os principais varejistas usam Gimbal para melhorar suas estratégias de “Omni-channel”, combater o “showrooming” e aumentar a fidelidade e as vendas.</h4>

					<p>
					Em um mundo onde o uso de celulares/smartphones é massivo, presente e essencial para quase todas as etapas da jornada de compras, os varejistas precisam repensar como as experiências digitais e físicas interagem entre si. Com o advento das soluções de marketing baseadas em localização, os varejistas hoje têm a oportunidade de recuperar a experiência do consumidor na loja, utilizando as capacidades de engajamento e análise de dados (analytics) que o celular cria. Analise seus consumidores, compreenda seus comportamentos e necessidades, entregue e envolva-se com eles através de meios similares aos que o mundo digital usa.<br><br>
					</p>
				</div>
				<div class="col-md-6">
						
						<img src="dist/imgs/negocios/icone-varejo.svg" width="45" class="iconVarejo01" alt="">

						<p class="centerTablet">Traga suas lojas físicas para a era digital e omni-channel com uma estratégia móvel baseada em localização. Crie experiências ricas para o consumidor com o objetivo de aumentar as visitas,  fidelidade, a freqüência e o valor das compras. Conecte verdadeiramente suas localizações físicas aos programas omni-channel e de recompensas.</p>

						<h4 class="text-center">84% Usam celular durante suas compra nas lojas</h4>

						<p class="text-center"><small><i>* Fonte: Google Shopper Marketing Agency Council and M/A/R/C Research</i></small></p>
				</div>
			</div>
		</div>
	</section>


	<section class="hospitalidade" id="hospitalidade">
		<div class="container">
			<div class="row">	
				<div class="col-xs-12">
					<img src="dist/imgs/negocios/icone-hospitalidade.svg" class="img-responsive iconVarejo02 hidden-md hidden-lg" width="244" alt="">
					<h2>Gimbal para hospitalidade</h2>
				</div>

				<div class="col-xs-12 col-md-6">
					
					<p class="centerTablet">Transforme as suas propriedades em engrenagens de fidelidade móveis movidos por localização.<br><br>Torne suas campanhas móveis um “Concierge” proativo de seus locais.<br><br>

					Crie experiências móveis relevantes e específicas para seus convidados para cada local especifico, entregando um melhor serviço, promovendo comodidades inéditas e por conseqüência aumentando a fidelidade. <br><br>

					Ative interações inteligentes e“sem-fricção” com seus clientes, que simplificarão suas operações e irão melhorar sua experiência enquanto estiverem em seus locais.
					</p>
				</div>
				<div class="col-xs-12 col-md-6 hidden-xs hidden-sm">
					<img src="dist/imgs/negocios/icone-hospitalidade.svg" class="img-responsive iconVarejo02" width="244" alt="">
				</div>
			</div>

		</div>

	</section>



	<section class="eventos" id="eventos">
		<div class="container">
			<div class="row">	
				<div class="col-xs-12">
					<img src="dist/imgs/negocios/icone-eventos.svg" width="263" class="img-responsive iconVarejo03 hidden-md hidden-lg" alt="">
					<h2>Gimbal para espaço de eventos</h2>
				</div>

				<div class="col-md-6">
					
					<p class="centerTablet">Transforme  o seu espaço com nossa tecnologia móvel e incremente o envolvimento dos participantes. <br><br>

					Forneça experiências ricas, em contexto e em tempo real para aumentar o engajamento, direcionar o tráfego para novas amenidades, aumentar as vendas de produtos, merchandising, serviços e melhorar substancialmente a navegação no local.<br><br>

					Gimbal atualmente fornece sua tecnologia para + de 60 equipes de esporte profissionais e universitários,ligas, operadores de locais e eventos.<p>

					<h4 class="centerTablet">Trate sempre seu público como "VIP"</h4>

					<p class="centerTablet">
					Campanhas Móveis são cada vez mais parte da experiência local. <br>
					Transforme-a de uma distração para um ativo. Entenda quem comparece aos seus eventos. <br>
					Envolva-se com eles no momento e no lugar certos dentro de seus locais.
					</p>
					
				</div>

				<div class="col-md-6 hidden-xs hidden-sm">
					<img src="dist/imgs/negocios/icone-eventos.svg" width="263" class="img-responsive iconVarejo03" alt="">
				</div>
			</div>

		</div>

	</section>



	<section class="financeiros" id="financeiros">
		
		<div class="bgCinzaInParallax">

			<div class="container">

				<div class="row">	
					<div class="col-xs-12">
						<img src="dist/imgs/negocios/icone-financeiros.svg" class="img-responsive grafico04 hidden-md hidden-lg" width="240" alt="">
						<h2>Gimbal para serviços financeiros</h2>
					</div>

					<div class="col-md-6">
						<h4 class="centerTablet">Torne Móvel a Experiência Bancária de Varejo</h4> <br>

						<p class="centerTablet">A economia centrada em “Big Data”  e nos dispositivos móveis está revolucionando quase todas as indústrias - incluindo serviços financeiros (bancos de varejo, seguros e muito mais). <br><br>

						Como os clientes tendem a preferir e buscar experiências inteligentes, convenientes e proativas, as empresas que trabalham para otimizar suas ofertas em todos os canais - particularmente móveis - têm a maior chance de encontrar novas oportunidades, clientes e fluxos de receita. <br><br>

						Com a principal solução de engajamento móvel e inteligência de localização no mercado, a Gimbal pode permitir que a Indústria de Serviços Financeiros crie experiências de “omni-channel” que criarão tráfego de transeuntes, oportunidades para aumento das vendas e fidelidade.</p>
					</div>

					<div class="col-md-6 hidden-xs hidden-sm">
						<img src="dist/imgs/negocios/icone-financeiros.svg" class="img-responsive grafico04" width="240" alt="">
					</div>
				</div>

			</div>

		</div>


		<div class="experienciasIcons" data-parallax="scroll" data-image-src="dist/imgs/backgrounds/parallax_negocios2.jpg" > 

			<div class="container">

				<div class="row"></div>
				
				<div class="row rowExperienciasIcons">
					<div class="col-sm-6 col-md-4 col-lg-4 iconFin01">
						<h5>ENVIE EXPERIÊNCIAS RELEVANTES</h5>
						<p>Combine os benefícios da compra on-line com locais físico-tradicionais, destacando itens, fornecendo comentários e muito mais.</p> 
					</div>

					<div class="col-sm-6 col-md-4 col-lg-4 iconFin02">
						<h5>ENTENDA OS FLUXOS DE TRÁFEGO</h5>
						<p>Reforce as análises de dados (analytics) da loja, entendendo melhor as horas de pico de clientes e as áreas internas mais freqüentadas.</p>
					</div>

					<div class="clearfix visible-sm-block"></div>

					<div class="col-sm-6 col-md-4 col-lg-4 iconFin03">
						<h5>AUMENTE VISITAS NAS LOJAS</h5>
						<p>Traga consumidores próximos a sua loja com ofertas e promoçoões especiais.</p> 
					</div>

				</div>
			</div>
		</div>
	</section>



	<section class="others">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="text-center">Nossa solução de marketing baseada em localização permite:</h3>
				</div>
			</div>

			<div class="row rowLocalizacao">
				<div class="col-md-6">
					<img src="dist/imgs/negocios/icon_localizacao01.svg" width="290" class="iconLocalz01" alt="">
				</div>

				<div class="col-md-6">
					<ul class="feature feat1">
						<li>GERAR TRÁFEGO DE TRANSEUNTES</li>
						<li>CRIAR EXPERIÊNCIAS VIP</li>
						<li>MELHORAR AS COMUNICAÇÕES COM OS CLIENTES</li>
						<li>AUMENTAR VISIBILIDADE DO CLIENTE </li>
						<li>MAXIMIZAR SATISFAÇÃO DO CLIENTE</li>
					</ul>
				</div>
			</div>

		</div>
	</section>


	<?php include '_footer.php'; ?>
	
<script src="dist/js/parallax.min.js"></script>

</body>

</html>