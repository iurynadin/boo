<?php 
$titulo = 'quem somos';
include '_meta.php';
?>

</head>

<body>

	<?php include '_header.php'; ?>


	<div class="slider">
		
		<div id="headerCarrousel" class="carousel slide" data-interval="false" data-wrap="true" data-ride="carousel" data-keyboard="true">

			<div class="carousel-inner" role="listbox">

				<div class="item item1-quemsomos active" id="0">
					
					<!-- <div class="holderItems">
						<div class="sliderTitle">Maximize sua estratégia móvel</div>
						<div class="sliderSubtitle">First class mobile experience at macro and micro locations</div>
						<p>Build, manage and analyse your mobiles campaings and location infrastructure</p>
					</div> -->

				</div>
				
				<!-- <div class="item item2" id="1">
					<div class="holderItems">
						<div class="sliderTitle">Título</div>
						<p>Subtitulo</p>
					</div>
				</div> -->	
			</div>

			<?php include '_slide_controls.php'; ?>

		</div>

	</div> 
	<!-- slider -->



	<section class="booParticipacoes">
		<!-- <div class="container">
			<div class="row">	
				<div class="col-xs-12">
					<h2>Geofênces de última geração</h2>
				</div>
			</div>

		</div> -->

		<!-- <div class="booParticipacoes"> -->

			<div class="container">
				
				<div class="row rowBooParticipacoes"> 

					<div class="col-xs-12">
						<h2>Boo Participações S.A.</h2>
						<p>BOO Participações S.A. é uma empresa que fornece:</p>
					</div>

					<div class="clearfix"></div>

					<div class="col-md-4 col-lg-4 colPartLocalizacao">
						<h3>INTELIGÊNCIA DE GEO LOCALIZAÇÃO</h3>
						<p>Inteligência de geolocalização para marcas e agências que querem encontrar os momentos mais adequados para comunicarem-se com os cidadãos.</p>
					</div>

					<!-- <div class="clearfix visible-sm-block visible-md-block"></div> -->

					<div class="col-md-4 col-lg-4 colPartApp">
						<h3>BOO! APP</h3>
						<p>Um aplicativo chamado BOO! que oferece experiências para atividades comerciais e culturais.</p> 
					</div>

					<div class="col-md-4 col-lg-4 colPartPublicidade">
						<h3>PUBLICIDADE MÓVEL</h3>
						<p>Um conjunto altamente integrado de soluções de publicidade móvel  para criação, amplificação de alcance, medições e insights que garantem maior desempenho do que qualquer outra plataforma de anúncios para celular.</p> 
					</div>

				</div>
			</div>

			
		<!-- </div> -->


	</section>






	<div class="container">
		<br>
		<h2>Blog</h2>

		<article class="post">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<a href="#">
						<img src="dist/imgs/backgrounds/blog.jpg" class="postImg img-responsive" alt="">
					</a>
					<span class="data">12.01.2017</span>
					<a href="#" class="postTitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas repudiandae iusto maxime laboriosam deserunt.</a>
					<div class="publisher">Publicado por <a href="#"><i>Charles Darwin</i></a></div>

					<div class="holderCateg">
						<a href="#" class="categoria">Tech</a>
						<a href="#" class="categoria">Evento</a>						
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem asperiores magnam veniam officiis, accusantium quisquam ducimus dolorem placeat dolores. Eius ab necessitatibus maxime voluptatibus repellat non incidunt totam porro doloribus ex, provident fugiat id facere corrupti veniam, inventore deleniti tenetur quo reiciendis. In repellat rerum ratione nihil quas fugiat, facilis autem magnam ipsam odit ab, quasi veniam reprehenderit impedit quae atque inventore esse.</p>

					<div class="blockquotes">
						Nostrum culpa sequi numquam, ab magnam impedit sit neque sapiente voluptatem nisi! Nobis commodi tempora repellat quos fuga! Minus, alias. Expedita doloribus, amet eius quisquam, officia perferendis maiores praesentium, laudantium nobis asperiores delectus nihil nam molestias.
						<span class="author">Autor da Citação</span>
					</div>

					<p>
					Cupiditate eius, reiciendis, rem labore consequuntur perspiciatis adipisci, totam ipsam debitis ex delectus laborum sapiente deleniti. Odit fugit nesciunt reprehenderit a aliquid itaque laborum eaque beatae non ea tempora sequi cum quis voluptates eos minus facilis, sint recusandae illum, qui suscipit perspiciatis libero enim iusto! Ad amet, id minus natus velit corrupti in suscipit rem placeat inventore voluptas, libero dignissimos, quibusdam modi laborum et temporibus blanditiis non expedita dolorum asperiores aspernatur? Fugit reiciendis porro aperiam, nostrum modi vel voluptatibus earum neque...&nbsp;&nbsp;<a href="#" class="lerMais">Continuar lendo</a></p>

					<br><br>
					
				</div>
			</div>
		</article>




		<article class="post">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<span class="data">12.01.2017</span>
					<a href="#" class="postTitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas repudiandae iusto maxime laboriosam deserunt.</a>
					<div class="publisher">Publicado por <a href="#"><i>Charles Darwin</i></a></div>
					<a href="#">
						<img src="dist/imgs/backgrounds/blog01.jpg" class="postImg postImgMt img-responsive" alt="">
					</a>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem asperiores magnam veniam officiis, accusantium quisquam ducimus dolorem placeat dolores. Eius ab necessitatibus maxime voluptatibus repellat non incidunt totam porro doloribus ex, provident fugiat id facere corrupti veniam, inventore deleniti tenetur quo reiciendis. In repellat rerum ratione nihil quas fugiat, facilis autem magnam ipsam odit ab, quasi veniam reprehenderit impedit quae atque inventore esse. <br><br>
					Cupiditate eius, reiciendis, rem labore consequuntur perspiciatis adipisci, totam ipsam debitis ex delectus laborum sapiente deleniti. Odit fugit nesciunt reprehenderit a aliquid itaque laborum eaque beatae non ea tempora sequi cum quis voluptates eos minus facilis, sint recusandae illum, qui suscipit perspiciatis libero enim iusto! Ad amet, id minus natus velit corrupti in suscipit rem placeat inventore voluptas, libero dignissimos, quibusdam modi laborum et temporibus blanditiis non expedita dolorum asperiores aspernatur? Fugit reiciendis porro aperiam, nostrum modi vel voluptatibus earum neque...&nbsp;&nbsp;<a href="#" class="lerMais">Continuar lendo</a></p>

					<hr>

					<div class="holderCateg">
						<a href="#" class="categoria">Tech</a>
						<a href="#" class="categoria">Evento</a>						
					</div>
					
					
				</div>
			</div>
		</article>
		
	</div>





	<section class="team">

		<div class="experienciasIcons" data-parallax="scroll" data-image-src="dist/imgs/backgrounds/parallax_roxo01.jpg" > 

			<div class="container">

				<div class="row">
					<div class="col-xs-12">
						<h2>Team</h2>
					</div>
				</div>
				
				<div class="row rowTeam">
					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Scott Meadow</h5>
						<div class="cargo">CEO</div>
						<p>Shopper Marketing, comunicação interativa, tecnologia da informação e gerenciamento de projetos.</p>
						<a href="mailto:scott.meadow@boo.social" class="emailOnPurple">scott.meadow@boo.social</a>
					</div>

					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Vinícius Neves</h5>
						<div class="cargo">COO</div>
						<p>Desenvolvimento de novos negócios, produção, planejamento e gerenciamento de projetos.</p>
						<a href="mailto:vinicius.neves@boo.social" class="emailOnPurple">vinicius.neves@boo.social</a>
					</div>


					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Eduardo Morelli</h5>
						<div class="cargo">CFO</div>
						<p>Expert em gestão financeira.</p>
						<a href="mailto:eduardo.morelli@boo.social" class="emailOnPurple">eduardo.morelli@boo.social</a> 
					</div>

					<div class="clearfix visible-sm-block visible-md-block visible-lg-block"></div>

					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Guilherme Franco</h5>
						<div class="cargo">DIRETOR DE NOVOS NEGOCIOS</div>
						<p>Vendas,estratégia de Midia digital e novos negócios.</p>
						<a href="mailto:guilherme.franco@boo.social" class="emailOnPurple">guilherme.franco@boo.social</a>
					</div>

					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Renato Salles</h5>
						<div class="cargo">COO</div>
						<p>Expert em Midia OOH, Desenvolvimento de Negócios & Investidor Anjo.</p> 
						<a href="mailto:renato.salles@boo.social" class="emailOnPurple">renato.salles@boo.social</a>
					</div>

					<div class="col-sm-4 col-md-4 iconTeam">
						<h5>Rubens Nigro</h5>
						<div class="cargo">CFO</div>
						<p>Expert em Midia OOH, Desenvolvimento de Negócios & Investidor Anjo.</p> 
						<a href="mailto:rubens.nigro@boo.social" class="emailOnPurple">rubens.nigro@boo.social</a>
					</div>

				</div>
			</div>
		</div>
	</section>




	<?php include '_footer.php'; ?>
	
<script src="dist/js/parallax.min.js"></script>

</body>

</html>