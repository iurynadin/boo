<?php 
$titulo = 'Nosso Aplicativo';
include '_meta.php';
?>

</head>

<body>

	<?php include '_header.php'; ?>


	<div class="slider">
		
		<div id="headerCarrousel" class="carousel slide" data-interval="false" data-wrap="true" data-ride="carousel" data-keyboard="true">

			<div class="carousel-inner" role="listbox">

				<div class="item item1-sdk" id="0">
					<a href="sdk.php">
						<?php include '_slide_sdk.php'; ?>
					</a>
				</div>

				<div class="item item1-home active" id="1">
					<?php include '_slide_app.php'; ?>
				</div>

				<div class="item item1-publicidademovel2" id="2">
					<?php include '_slide_publicidademovel.php'; ?>
				</div>
				
				
			</div>

			<?php include '_slide_controls.php'; ?>

		</div>

		<a href="#grandesdesafios" class="sliderDown">down</a>

	</div> 
	<!-- slider -->


	<?php include "_menuinterna_boo.php"; ?>


	<section class="booGrandesDesafios" id="grandesdesafios">

			<div class="container">
				
				<div class="row rowBooParticipacoes"> 

					<br><br>
					<h6 class="text-center">UM APLICATIVO CHAMADO BOO! QUE OFERECE EXPERIÊNCIAS PARA ATIVIDADES COMERCIAIS E CULTURAIS.</h6>
					<br><br><br>

					<div class="col-xs-12">
						<h2>Grande Desafio<span>Novos Meios e a Publicidade Antiga</span></h2>
					</div>

					<div class="clearfix"></div>

					<div class="col-md-3 colDesafio1">
						<h3>Bens de consumo</h3>
						<p>70% das decisões de compras são feitas no PDV ou próximo a ele.</p> 
						<div class="mais"></div>
					</div>

					<!-- <div class="clearfix visible-sm-block visible-md-block"></div> -->

					<div class="col-md-3 colDesafio2">
						<h3>Móvel</h3>
						<p>As aplicações móveis estão disponíveis para orientar e promover as comunicações das marcas. Estas APPs tornaram-se amplamente utilizadas. </p>
						<div class="mais"></div>
					</div>

					<div class="col-md-3 colDesafio3">
						<h3>Proximidade</h3>
						<p>Até recentemente, havia um elemento-chave ausente nas comunicações: Informações de geolocalização precisas e confiáveis que produzem oportunidades inigualáveis no marketing contextual.</p> 
						<div class="igual"></div>
					</div>

					<div class="col-md-3 colDesafio4">
						<h3>Bens de consumo</h3>
						<p>70% das decisões de compras são feitas no PDV ou próximo a ele.</p>
					</div>

				</div>


				<div class="row">
					<div class="col-xs-12">
						<h5 class="text-center">Oferecemos comunicações de marketing com conteúdo persuasivo suportado pela tecnologia BEACON.</h5>
					</div>
				</div>
			</div>

	</section>



	<section class="booFornece">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-7 centerMobile">

					<img src="dist/imgs/ui/logo.svg" width="100" alt="Logo Boo">

					<h6 class="uppercase">Para empresas que não tem um aplicativo, BOO! Fornece</h6> 

					<h4>SOFTWARE + BANCO DE DADOS</h4>

					<h6 class="uppercase">Para melhor utilização de marketing de proximidade.</h6>
				</div>

				<div class="col-md-4 col-lg-5">
					<img src="dist/imgs/app/imgapp1.jpg" height="542" width="305" class="img-responsive appImage" alt="">
				</div>

			</div>
		</div>
	</section>



	<section class="imgExperiences">

		<div class="appParalax" data-parallax="scroll" data-image-src="dist/imgs/backgrounds/app_parallax1.jpg" > 

			<div class="container">

				<div class="row relative">

					<div class="col-xs-12">
	
						<div class="content1">
							<p>Imagine um companheiro para as suas viagens diárias que irá aprender e lembrar sobre seus interesses e aspirações;
							que irá ajudá-lo a evitar a inundação de informações indesejadas; 
							que irá guiá-lo para o lugar exato onde  você pode encontrar o que você está procurando.
							</p>
						</div>

						<div class="content2">
							<p>Em casa ou no trabalho, você pode planejar o seu dia, o seu fim de semana, a sua viagem e saber tudo sobre dicas culturais, comerciais e de serviços que estão disponíveis ao seu redor. </p>
						</div>
						
					</div>

				</div>
				
			</div>
		</div>



		<div class="appParalax" data-parallax="scroll" data-image-src="dist/imgs/backgrounds/app_parallax2.jpg" > 

			<div class="container">

				<div class="row contentParallax2">

					<div class="col-xs-12 col-md-2">
						<img src="dist/imgs/app/iconimage02.svg" width="150" alt="" class="iconImgApp">
					</div>

					<div class="col-xs-12 col-md-9">
						<p>Essas experiências consistem em conteúdo  original e editado de outras fontes, que são entregues usando um tom e maneira conversacional.</p> 
					</div>

				</div>
				
			</div>
		</div>


		<div class="appParalax" data-parallax="scroll" data-image-src="dist/imgs/backgrounds/app_parallax3.jpg" > 

			<div class="container">

				<div class="row contentParallax3">

					<div class="col-xs-12 col-md-2">
						<img src="dist/imgs/app/iconimage03.svg" width="150" alt="" class="iconImgApp">
					</div>

					<div class="col-xs-12 col-md-9 col-lg-6">
						<p>Envolvemos as pessoas ligando sua proximidade a pontos de interesse.</p> 
					</div>

				</div>
				
			</div>
		</div>
	</section>



	<section class="viagens">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-2">
					<img src="dist/imgs/app/icon-app5.svg" width="80" alt="" class="iconImgApp">
				</div>
				<div class="col-md-9 col-lg-8">
					<h6>
						USANDO os BEACONS e o SDK da GIMBAL™<br>
						Criamos EXPERIÊNCIAS que as pessoas podem usufruir em suas VIAGENS.
					</h6>
				</div>
			</div>

			<br><br>
			
			<div class="row">
				<div class="col-md-6">
					<img src="dist/imgs/app/imgapp2.jpg" height="320" width="390" class="img-responsive ImgApp2 " alt="">
				</div>
				<div class="col-md-6">
					<h6 class="contentH6 centerMobile" style="line-height: 1.4;">Estas experiências estão disponíveis para: <br><br>
					Centros comerciais <br>
					Supermercados <br><br>

					Estamos planejando: <br>
					Rede cidade <br>
					Transporte urbano <br>
					Aeroportos <br>
					Arenas (Esportes e outros entretenimentos) <br>
					Museus (Galerias de Arte, também) <br>
					</h6>
				</div>
			</div>

			<br><br>

			<div class="row">
				<div class="col-xs-12 text-center">
					<h6>Estes são alguns exemplos de experiências que o BOO pode criar em locais de interesse:</h6>
				</div>

				<div class="col-sm-4 text-center">
					<h6>SHOPPING</h6>
				</div>
				<div class="col-sm-4 text-center">
					<h6>SUPERMERCADO</h6>
				</div>
				<div class="col-sm-4 text-center">
					<h6>AEROPORTO</h6>
				</div>
			</div>

		</div>
	</section>



	<section class="rowCell">
		<div class="container">
			<div class="row">
				<div class="col-md-8 cellText">

					<h6 class="uppercase h61">NÓS TEMOS TECNOLOGIA ÚNICA QUE UTILIZA</h6>

					<h4 class="uppercase h41">PROXIMIDADE E CONTEXTO</h4>

					<h6 class="uppercase h62">PARA ENTREGAR CONTEÚDO E OFERTAS PARA CIDADÃOS</h6>

					<h4 class="uppercase h42">COM BASE EM SEUS DESEJOS E COMPORTAMENTOS.</h4>

				</div>
				<div class="col-md-4">
					<img src="dist/imgs/app/cell.jpg" height="353" width="300" alt="">
				</div>
			</div>
		</div>
	</section>




	<?php include '_footer.php'; ?>
	
<script src="dist/js/parallax.min.js"></script>

</body>

</html>