GRUNT

1. npm init
2. sudo npm install grunt --save-dev
3. Criar Gruntfile.js

module.exports = function (grunt){

	grunt.initConfig({

	});

	grunt.registerTask('default', []);

}

4. Instalando grunt contrib e outras tasks necessaria
sudo npm install grunt-contrib-jshint --save-dev
sudo npm install jquery
sudo npm install grunt-contrib-concat --save-dev
sudo npm install bootstrap
sudo npm install grunt-contrib-uglify --save-dev
sudo npm install grunt-contrib-cssmin --save-dev
sudo npm install --save-dev grunt-sass
sudo npm install grunt-contrib-clean --save-dev
sudo npm install grunt-contrib-watch --save-dev
sudo npm install grunt-browser-sync --save-dev



GULP
sudo npm install -g gulp - instalando globalmente

sudo npm install gulp --save-dev

sudo npm install jshint gulp-jshint --save-dev

sudo npm install gulp-concat --save-dev

sudo npm install --save-dev gulp-uglify

sudo npm install --save-dev gulp-clean

sudo npm install --save-dev gulp del

sudo npm install gulp-minify-css --save-dev

sudo npm install gulp-autoprefixer --save-dev

sudo npm install gulp-notify --save-dev

sudo npm install gulp gulp-sass --save-dev

sudo npm install gulp gulp-sourcemaps --save-dev

sudo npm install browser-sync gulp --save-dev

código final gulpfile.js:

var gulp = require('gulp');

var minifycss = require('gulp-minify-css');
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
//var notify = require('gulp-notify');


gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch("scss/*.scss", ['sass']);
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('sass', function(){

	return gulp.src('scss/main.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'compressed'
			}))
		.pipe(autoprefixer('last 15 version'))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest('css'))
		.pipe(browserSync.stream());
		//.pipe(notify({message: 'Feito'}));

});

gulp.task('default', ['serve']);



npm view jquery versions
npm install jquery@1.12.4