<?php 
$titulo = 'Home';
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Language" content="pt-br">
	<meta name="description" content="Boo">
	<meta name="keywords" content="">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:url">
	<meta name="author" content="Tela Plana" />
	<meta name="robots" content="index,follow">
	<meta name="revisit-after" content="2 DAYS" />
	<meta name="resource-type" content="document" />
	<meta name="distribution" content="Global" />
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<link rel="icon" href="favicon.ico" type="image/x-icon"> 
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<title>Boo<?php if($titulo) { echo ' - ' . $titulo; } ?></title>

	<link rel="stylesheet" href="dist/css/styles.min.css?v=1.3">
	<script src="bower_components/jquery/dist/jquery.2.min.js"></script>
	<script src="dist/js/parallax.min.js"></script>
	<script src="bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.min.js"></script>

</head>

<body>

	
	<div data-parallax="scroll" data-image-src="dist/imgs/backgrounds/solucoes_experiencias.jpg" data-z-index="1"> 

		<br><br><br><br><br><br>

	</div>


	<?php include '_footer.php'; ?>

	<script>

		(function($){


		})(jQuery);
	</script>
	
</body>

</html>