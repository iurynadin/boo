<?php 
$titulo = 'Blog';
include '_meta.php';
?>

</head>

<body>

	<?php include '_header.php'; ?>

	<section class="blogHeader" data-parallax="scroll" data-image-src="dist/imgs/slider/blog.jpg">
		<div class="container">
			<div class="row">
				<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
			</div>
		</div>
	</section>



	<div class="container">

		<article class="post">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<span class="data">12.01.2017</span>
					<a href="#" class="postTitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas repudiandae iusto maxime laboriosam deserunt.</a>
					<div class="publisher">Publicado por <a href="#"><i>Charles Darwin</i></a></div>

					<a href="#">
						<img src="dist/imgs/backgrounds/blog.jpg" class="postImg img-responsive" alt="">
					</a>

					<div class="holderCateg">
						<a href="#" class="categoria">Tech</a>
						<a href="#" class="categoria">Evento</a>						
					</div>

					<hr>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem asperiores magnam veniam officiis, accusantium quisquam ducimus dolorem placeat dolores. Eius ab necessitatibus maxime voluptatibus repellat non incidunt totam porro doloribus ex, provident fugiat id facere corrupti veniam, inventore deleniti tenetur quo reiciendis. In repellat rerum ratione nihil quas fugiat, facilis autem magnam ipsam odit ab, quasi veniam reprehenderit impedit quae atque inventore esse.</p>

					<div class="blockquotes">
						Nostrum culpa sequi numquam, ab magnam impedit sit neque sapiente voluptatem nisi! Nobis commodi tempora repellat quos fuga! Minus, alias. Expedita doloribus, amet eius quisquam, officia perferendis maiores praesentium, laudantium nobis asperiores delectus nihil nam molestias.
						<span class="author">Autor da Citação</span>
					</div>

					<p>
					Cupiditate eius, reiciendis, rem labore consequuntur perspiciatis adipisci, totam ipsam debitis ex delectus laborum sapiente deleniti. Odit fugit nesciunt reprehenderit a aliquid itaque laborum eaque beatae non ea tempora sequi cum quis voluptates eos minus facilis, sint recusandae illum, qui suscipit perspiciatis libero enim iusto! Ad amet, id minus natus velit corrupti in suscipit rem placeat inventore voluptas, libero dignissimos, quibusdam modi laborum et temporibus blanditiis non expedita dolorum asperiores aspernatur? Fugit reiciendis porro aperiam, nostrum modi vel voluptatibus earum neque...&nbsp;&nbsp;<a href="#" class="lerMais">Continuar lendo</a></p>

					<br><br>
					
				</div>
			</div>
		</article>




		<article class="post">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<span class="data">12.01.2017</span>
					<a href="#" class="postTitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas repudiandae iusto maxime laboriosam deserunt.</a>
					<div class="publisher">Publicado por <a href="#"><i>Charles Darwin</i></a></div>
					<a href="#">
						<img src="dist/imgs/backgrounds/blog01.jpg" class="postImg postImgMt img-responsive" alt="">
					</a>

					<div class="holderCateg">
						<a href="#" class="categoria">Tech</a>
						<a href="#" class="categoria">Evento</a>						
					</div>
					<hr>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem asperiores magnam veniam officiis, accusantium quisquam ducimus dolorem placeat dolores. Eius ab necessitatibus maxime voluptatibus repellat non incidunt totam porro doloribus ex, provident fugiat id facere corrupti veniam, inventore deleniti tenetur quo reiciendis. In repellat rerum ratione nihil quas fugiat, facilis autem magnam ipsam odit ab, quasi veniam reprehenderit impedit quae atque inventore esse. <br><br>
					Cupiditate eius, reiciendis, rem labore consequuntur perspiciatis adipisci, totam ipsam debitis ex delectus laborum sapiente deleniti. Odit fugit nesciunt reprehenderit a aliquid itaque laborum eaque beatae non ea tempora sequi cum quis voluptates eos minus facilis, sint recusandae illum, qui suscipit perspiciatis libero enim iusto! Ad amet, id minus natus velit corrupti in suscipit rem placeat inventore voluptas, libero dignissimos, quibusdam modi laborum et temporibus blanditiis non expedita dolorum asperiores aspernatur? Fugit reiciendis porro aperiam, nostrum modi vel voluptatibus earum neque...&nbsp;&nbsp;<a href="#" class="lerMais">Continuar lendo</a></p>


					
					
				</div>
			</div>
		</article>


		<div class="row text-center">
			<nav aria-label="Page navigation">
			  <ul class="pagination">
			    <li>
			      <a href="#" aria-label="Previous">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>
			    <li><a href="#">1</a></li>
			    <li><a href="#">2</a></li>
			    <li><a href="#">3</a></li>
			    <li><a href="#">4</a></li>
			    <li><a href="#">5</a></li>
			    <li>
			      <a href="#" aria-label="Next">
			        <span aria-hidden="true">&raquo;</span>
			      </a>
			    </li>
			  </ul>
			</nav>
			<br><br>
		</div>
		

	</div>


	<?php include '_footer.php'; ?>
	
<script src="dist/js/parallax.min.js"></script>

</body>

</html>