var gulp = require('gulp');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var del = require('del'); //no lugar de gulp-clean
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-cssmin');
// var htmlmin = require('gulp-htmlmin');
//var cleanCSS = require('gulp-clean-css');
var runSequence = require('run-sequence')
var connect = require('gulp-connect-php')
var browserSync = require('browser-sync').create();

gulp.task('jshint', function() {
    return gulp.src('js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('cssmin',['sass'] , function() {
    return gulp.src('css/**/*.css')
    //.pipe(cleanCSS())
    .pipe(concat('styles.min.css'))
    .pipe(cssmin())
    .pipe(gulp.dest('dist/css'));
});

//gulp.task('uglify', ['clean'], function() {
gulp.task('uglify', function() {
    return gulp.src(['node_modules/jquery/dist/jquery.min.js','node_modules/jquery/dist/jquery.easing.min.js','node_modules/bootstrap/dist/js/bootstrap.min.js','js/**/*.js']) //aqui no source é possivel ter varias fontes, js
    .pipe(uglify())
    .pipe(concat('all.min.js')) //trocar o concat pelo uglify
    .pipe(gulp.dest('dist/js'));
});

gulp.task('connect-sync', function() {
    connect.server({}, function (){
        browserSync.init({
            proxy: '127.0.0.1:8000',
            startPath: "index.php"
        });
    });
    gulp.watch(["scss/*.scss","node_modules/bootstrap-sass/assets/stylesheets/bootstrap/_variables.scss"], ['cssmin']).on('change', browserSync.reload);
    gulp.watch(["js/*.js"], ['uglify']).on('change', browserSync.reload);
    gulp.watch("*.php").on('change', browserSync.reload);
});


gulp.task('serve', function() {
    browserSync.init({
        server: "./",
        startPath: "index.php"
    });
    gulp.watch(["scss/*.scss","node_modules/bootstrap-sass/assets/stylesheets/bootstrap/_variables.scss"], ['cssmin']).on('change', browserSync.reload);
    gulp.watch("*.php").on('change', browserSync.reload);
});

gulp.task('default', function(cb) {
    //return runSequence('clean' , ['jshint', 'uglify' , 'cssmin'], 'serve' , cb)
    return runSequence(['jshint', 'uglify' , 'cssmin' ], 'connect-sync' , cb)
});


