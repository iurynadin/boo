<div class="holderItems">
	<div class="sliderTitle">Plataforma de dados de localização móvel</div>
	<div class="sliderSubtitle">Entregue anúncios personalizados baseados em localização e proximidade</div>

	<div class="holderMainIcon hidden-xs">
		<img src="dist/imgs/slider/icon/icon_sdk_novo.svg" width="95" alt="">
	</div>

	<div class="holderSecondIcon hidden-xs">
		<img src="dist/imgs/slider/icon/icon_sdk_secundario.svg" width="80" alt="">
	</div>

</div>