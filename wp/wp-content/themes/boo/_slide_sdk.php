<div class="holderItems">
	<div class="sliderTitle">Plataforma de dados de localização móvel</div>
	<div class="sliderSubtitle">Entregue anúncios personalizados baseados em localização e proximidade</div>

	<div class="holderMainIcon hidden-xs">
		<img src="<?php bloginfo('template_url'); ?>/imgs/slider/icon/icon_sdk_novo.svg" width="110" alt="">
	</div>

	<div class="holderSecondIcon hidden-xs">
		<img src="<?php bloginfo('template_url'); ?>/imgs/slider/icon/icon_sdk_secundario.svg" width="90" alt="">
	</div>

</div>