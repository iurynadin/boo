<?php get_header(); ?>

<section class="blogHeader" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/imgs/backgrounds/blog.jpg">
	<div class="container">
		<div class="row">
			<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
		</div>
	</div>
</section>


<div class="container">
	<br>
	<?php 

		if(have_posts()) : ?>

			<h3><?php 
				if(is_category()){
					echo 'Categoria: ';
					single_cat_title();
				} elseif(is_tag()){
					echo 'Tag: '; single_cat_tag();
				} elseif(is_author()){
					the_post();
					echo 'Autor: ' . get_the_author();
					rewind_posts();
				} elseif(is_day()){
					echo 'Arquivos diários: ' . get_the_date();
				} elseif(is_month()){
					echo 'Mês: '. get_the_date('m Y');
				} elseif(is_year()){
					echo 'Ano: '. get_the_date('Y');;
				} else {
					echo 'Arquivos: ';
				}
			 ?></h3>

		<?php while(have_posts()) : the_post();

				include '_part_post.php';

			endwhile; 

				echo paginate_links();

			else : ?>

		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

	<?php endif; ?>


</div>


<?php get_footer(); ?>