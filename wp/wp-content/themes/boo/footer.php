<a href="#" class="backtotop">backtotop</a>


<section class="newsletter">
	<div class="container">
		
		<div class="row">

			<div class="col-md-6 col-md-offset-3 text-center">

				<button type="button" class="btnContato" id="btnModal">
				  Entre em contato
				  <span>para mais informações</span>
				</button>
				
			</div>

		</div>

	</div>			

</section>


<footer>
	<div class="container">
			
			<div class="footerLogo">
				<img src="<?php bloginfo('template_url'); ?>/imgs/ui/logo_negativo.svg" width="55" alt="">
			</div>

			<div class="footerMenu">

				<ul class="footerBoo">
					<li><a href="<?php echo get_home_url(); ?>/<?php echo get_home_url(); ?>">Quem Somos</a></li>
			        <li><a href="<?php echo get_home_url(); ?>/sdk">Nosso Software</a></li>
			        <li><a href="<?php echo get_home_url(); ?>/app">Nosso Aplicativo</a></li>
			        <li><a href="<?php echo get_home_url(); ?>/publicidademovel">Publicidade Móvel</a></li>
			        <li><a href="<?php echo get_home_url(); ?>/blog">Blog</a></li>
				</ul>

				<ul class="footerGimbal">
					<li><a href="<?php echo get_home_url(); ?>/solucoes">Soluções</a></li>
					<li><a href="<?php echo get_home_url(); ?>/negocios">Negócios</a></li>
					<li><a href="<?php echo get_home_url(); ?>/publishers">Publishers</a></li>
					<li><a href="<?php echo get_home_url(); ?>/anunciantes">Anunciantes</a></li>
					<li><a href="http://www.gimbal.com/developers/" target="blank">Developers</a></li>
				</ul>

			</div>

			<div class="clear"></div>
			
			<ul class="footerRedes">
				<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/imgs/ui/footer_youtube.svg" width="34" alt=""></a></li>
				<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/imgs/ui/footer_twitter.svg" width="34" alt=""></a></li>
				<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/imgs/ui/footer_instagram.svg" width="34" alt=""></a></li>
				<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/imgs/ui/footer_face.svg" width="34" alt=""></a></li>
			</ul>

			<!-- <ul class="footerTerms">
				<li><a href="#">Privacy Police</a></li>
				<li><a href="#">Termos & Condições</a></li>
				<li><a href="#">Cookie Police</a></li>
			</ul> -->

			<ul class="copyrights">
				<li><img src="<?php bloginfo('template_url'); ?>/imgs/ui/copyr.svg" width="10" alt=""></li>
				<li><span>Gimbal Technologies</span></li>
				<li><p>BOO! Participações  todos os direitos reservados</p></li>
			</ul>
	</div>
</footer>



<!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Contato</h4>
			</div>

			<div class="modal-body">
				
				<div class="row">
					<div class="col-xs-12">
						<p>Entre em contato para saber mais sobre as soluções da Boo.<br>
							Guilherme Franco - <a href="mailto:guilherme.franco@boo.social">guilherme.franco@boo.social</a>
							<br>Phone: +55 11 98137-0793
						</p>
						<br>
					</div>
				</div>



				<form name="sentMessage" id="contactForm" data-toggle="validator" role="form">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="name">Nome*</label>
								<input type="text" class="form-control" id="name" name="name" data-error="Necessário nome!" required>
								<div class="help-block with-errors"></div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="email">Email*</label>
								<input type="email" class="form-control" id="email" name="email" data-error="Necessário o email!" required>
								<div class="help-block with-errors"></div>
							</div>
						</div>

						<div class="clear"></div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="empresa">Empresa</label>
								<input type="text" name="empresa" class="form-control">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="assunto">Assunto</label>
								<input type="text" name="assunto" class="form-control">
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="email">Mensagem*</label>
								<textarea class="form-control" id="message" rows="8"></textarea>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-lg-12 text-center">
							<div id="success"></div>
							<button type="submit" class="btn">Enviar</button>
						</div>
					</div>
				</form>

			</div>

			<!-- <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div> -->
		</div>
	</div>
</div>



<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script> -->
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
<?php wp_footer(); ?>

</body>
</html>