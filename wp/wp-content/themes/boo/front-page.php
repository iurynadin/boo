<?php get_header(); ?>

<div class="video">
	<iframe width="1060" height="596" src="https://www.youtube.com/embed/ebQm1X_lEbk?feature=oembed&rel=0&amp;showinfo=0" frameborder="0" allowfullscreen=""></iframe>
</div>

<?php include "_menuinterna_boo.php"; ?>

<div class="container">
	<br>
	<h2>Blog</h2>


	<?php 

		$lastCases = new WP_Query('cat=2&posts_per_page=2');

		if($lastCases->have_posts()) :

			while($lastCases->have_posts()) : $lastCases->the_post();

				include '_part_post.php';

			endwhile;

			else : 
				echo '<p>Nenhuma postagem</p>';

		endif; ?>

			<div class="row text-center">
				
				<?php echo '<a href="'. get_category_link(2) .'" class="verTodos">Ver posts anteriores</a>'; ?>

			</div>

		<?php wp_reset_postdata(); ?>
	
</div>


<section class="team">

	<div class="experienciasIcons" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/imgs/backgrounds/parallax_team.jpg" > 

		<div class="container">

			<div class="row">
				<div class="col-xs-12">
					<h2>Time</h2>
				</div>
			</div>
			
			<div class="row rowTeam">
				<div class="col-sm-4 col-md-4 iconTeam">
					<h5>Scott Meadow</h5>
					<div class="cargo">CEO</div>
					<p>Shopper Marketing, comunicação interativa, tecnologia da informação e gerenciamento de projetos.</p>
					<a href="mailto:scott.meadow@boo.social" class="emailOnPurple">scott.meadow@boo.social</a>
				</div>

				<div class="col-sm-4 col-md-4 iconTeam">
					<h5>Vinícius Neves</h5>
					<div class="cargo">COO</div>
					<p>Desenvolvimento de novos negócios, produção, planejamento e gerenciamento de projetos.</p>
					<a href="mailto:vinicius.neves@boo.social" class="emailOnPurple">vinicius.neves@boo.social</a>
				</div>


				<div class="col-sm-4 col-md-4 iconTeam">
					<h5>Eduardo Morelli</h5>
					<div class="cargo">CFO</div>
					<p>Expert em gestão financeira.</p>
					<a href="mailto:eduardo.morelli@boo.social" class="emailOnPurple">eduardo.morelli@boo.social</a> 
				</div>

				<div class="clearfix visible-sm-block visible-md-block visible-lg-block"></div>

				<div class="col-sm-4 col-md-4 iconTeam">
					<h5>Guilherme Franco</h5>
					<div class="cargo">DIRETOR DE NOVOS NEGOCIOS</div>
					<p>Vendas,estratégia de Midia digital e novos negócios.</p>
					<a href="mailto:guilherme.franco@boo.social" class="emailOnPurple">guilherme.franco@boo.social</a>
				</div>

				<div class="col-sm-4 col-md-4 iconTeam">
					<h5>Renato Salles</h5>
					<!-- <div class="cargo">COO</div> -->
					<p>Expert em Midia OOH, Desenvolvimento de Negócios & Investidor Anjo.</p> 
					<a href="mailto:renato.salles@boo.social" class="emailOnPurple">renato.salles@boo.social</a>
				</div>

				<div class="col-sm-4 col-md-4 iconTeam">
					<h5>Rubens Nigro</h5>
					<!-- <div class="cargo">CFO</div> -->
					<p>Expert em Midia OOH, Desenvolvimento de Negócios & Investidor Anjo.</p> 
					<a href="mailto:rubens.nigro@boo.social" class="emailOnPurple">rubens.nigro@boo.social</a>
				</div>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>