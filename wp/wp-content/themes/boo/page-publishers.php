<?php get_header(); ?>

<div class="slider">
		
	<div id="headerCarrousel" class="carousel slide" data-interval="false" data-wrap="true" data-ride="carousel" data-keyboard="true">

		<div class="carousel-inner" role="listbox">

			<div class="item item1-negocios" id="0">
				<a href="negocios">
					<?php include '_slide_negocios.php'; ?>
				</a>
			</div>

			<div class="item item1-publisher active" id="1">
				<?php include '_slide_publishers.php'; ?>
			</div>

			<div class="item item1-anunciantes" id="2">
				<a href="anunciantes">
					<?php include '_slide_anunciantes.php'; ?>
				</a>
			</div>
			
		</div>

		<?php include '_slide_controls.php'; ?>

	</div>

	<a href="#publishers" class="sliderDown">down</a>

</div> 
<!-- slider -->



<section class="publishers" id="publishers">
	<div class="container">
		<div class="row">	
			<div class="col-xs-12">
				<img src="<?php bloginfo('template_url'); ?>/imgs/publishers/publisher01.svg" width="400" class="img-responsive iconPub01 hidden-md hidden-lg" alt="">
				<h2>Gimbal para publishers</h2>
			</div>

			<div class="col-xs-12 col-md-7">
				
				<p class="centerTablet">Um único SDK. Localização abrangente e engajamento via proximidade.
				<br><br>Use Gimbal para entender a jornada de seus usuários no mundo físico como nunca foi possível anteriormente. Encontre novas maneiras de participar e rentabilizar sua audiência de aplicativos para dispositivos móveis.<br><br>

				Ao usar os “SDKs” e softwares leves, confiáveis e potentes da Gimbal, os “publishers” tem a capacidade de entender melhor o comportamento, as macro e micro-localizações de seus usuários no mundo real, ao mesmo tempo em que oferecem uma experiência móvel aprimorada e personalizada. Os “publishers” que se inscrevem na Plataforma de Dados de Proximidade da Gimbal também têm a capacidade coletar dados de localização proprietários compatíveis com privacidade e também em participar da troca de valores monetizada desses dados. <br><br>

				É muito claro que chegou a hora para os “publishers” coletarem e aproveitarem melhor os dados de localização para incrementarem as mais relevantes experiências do usuário dos seus aplicativos e por conseqüência aumentar os fluxos de monetização. A Gimbal fornece aos “publishers” uma plataforma completa de engajamento e localização  com intuito de aprimorar as experiências dos usuários, inteligente para dispositivos móveis descobrir novas percepções (insights) de seu público-alvo e desbloquear novos fluxos de receita. <br><br> 

				A Gimbal capacita os “publishers” à entender melhor seus usuários, permitir uma segmentação de público mais relevante e precisa, como também em abrir novas oportunidades estratégicas de receita. <br><br>

				Aproveite a plataforma líder de mercado da Gimbal para aprimorar sua experiência de usuário
				</p>
			</div>
			<div class="col-xs-12 col-md-5 hidden-xs hidden-sm">
				<img src="<?php bloginfo('template_url'); ?>/imgs/publishers/publisher01.svg" width="400" class="img-responsive iconPub01" alt="">
			</div>
		</div>

	</div>

</section>



<section class="pubPlataformas" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/imgs/backgrounds/parallax_publisher.jpg">
	<div class="container">
		<div class="row">
			<div class="col-md-6 plat01">
				<h3>PLATAFORMA DE LOCALIZAÇÃO MÓVEL</h3>
				<p class="texto">Forneça conteúdo contextual para seus usuários com base em suas localizações;<br>
				Ative usuários inativos com base em sua localização com notificações de“push”personalizadas; <br>
				Melhore o seu relacionamento com os seus usuários através de uma compreensão mais profunda do seu comportamento via geo-localização; <br>
				Crie experiências personalizadas e que atendam todas as necessidades do consumidor em minutos.
				</p>
			</div>
			<div class="col-md-6 plat02">
				<h3>PLATAFORMA DE DADOS DE PROXIMIDADE</h3>
				<p class="texto">Desbloqueie um novo fluxo de receitas sem afetar sua experiência de usuário; <br>
				Experimente um aumento do CPM e um ganho em vendas através da utilização de dados proprietáriose precisos de localização; <br>
				Dados de localização anônimos e compatíveis com privacidade são usados para segmentação e monetização de audiência; <br> 
				Monetização de atribuições em ambientes físicos; <br>
				Obtenha informações de localização mais profundas sobre seus usuários.
				</p>
			</div>
		</div>
	</div>
</section>



<section class="others">
	<div class="container">

		<div class="row">
			<div class="col-xs-12">
				<h3 class="text-center">Você pode usar o Gimbal para:</h3>
			</div>
		</div>

		<div class="row rowLocalizacao">
			<div class="col-md-6">
				<img src="<?php bloginfo('template_url'); ?>/imgs/negocios/icon_localizacao02.svg" width="290" class="iconLocalz02" alt="">
			</div>

			<div class="col-md-6">
				<ul class="feature feat3">
					<li>AUMENTE A PERCEPÇÃO DE SEUS LOCAIS OU PRODUTOS MAIS PRÓXIMOS</li>
					<li>ENTREGE CUPONS OU OFERTAS A DISPOSITIVOS MÓVEIS EM PONTOS DE INTERESSE </li>
					<li>FORNEÇA ATRIBUIÇÕES EM AMBIENTES FÍSICOS</li>
					<li>AUMENTE O FLUXO DE TRANSEUNTES PARA SEUS LOCAIS/NEGÓCIOS </li>
					<li>INTERAJA COM SEUS PÚBLICO DESEJADO DE FORMA PERSONALIZADA E SIGNIFICATIVA </li>
				</ul>
			</div>
		</div>

	</div>
</section>

<?php get_footer(); ?>