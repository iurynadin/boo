<?php 

function boo_resources(){
	wp_enqueue_style('style', get_stylesheet_uri());
	// wp_enqueue_script( 'main.js', get_template_directory_uri() . '/js/main.js', array(), true );
	wp_enqueue_script( 'validator.js', get_template_directory_uri() . '/js/validator.js', array(), true );
	wp_enqueue_script( 'common_scripts_min.js', get_template_directory_uri() . '/js/common_scripts_min.js', array(), true );
}
add_action('wp_enqueue_scripts', 'boo_resources');


//theme setup
function booSetup()
{
	//menu
	register_nav_menus(array(
		'principal' => __('Menu principal')
		// 'projetos' => __('Menu projetos')
	));
	//adicionando featured image support
	add_theme_support('post-thumbnails'); 
	// add_image_size('small-thumbnail', 368 , 250, true );
	add_image_size('post-image' , 750 );
}
add_action('after_setup_theme', 'booSetup');



//paginação com números (usada)
function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $total_pages,
        	'prev_text'          => __('<'),
			'next_text'          => __('>'),
        ));
    }
}