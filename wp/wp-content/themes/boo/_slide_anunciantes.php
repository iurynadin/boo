<div class="holderItems">
	<div class="sliderTitle">Inteligência de localização e proximidade</div>
	<div class="sliderSubtitle"> Crie experiências para celular únicas e relevantes  em macro e micro-localizações.</div>

	<div class="holderMainIcon hidden-xs">
		<img src="<?php bloginfo('template_url'); ?>/imgs/slider/icon/icon_home.svg" width="120" alt="">
	</div>

	<div class="holderSecondIcon hidden-xs">
		<img src="<?php bloginfo('template_url'); ?>/imgs/slider/icon/icon_anunciantes.svg" width="82" alt="">
	</div>

	<p class="hidden-xs hidden-sm">Aproveite a plataforma líder de mercado da Gimbal para aprimorar sua experiência de usuário</p>
</div>