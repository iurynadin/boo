var menuresp = false;

(function($){

    var scrollCount = 0, 
        latestScrollTop = 0,
        doc = document.documentElement,
        top = 0;

	$.extend($.easing, {
        easeInOutCubic : function(x, t, b, c, d){
            if ((t/=d/2) < 1) return c/2*t*t*t + b;
            return c/2*((t-=2)*t*t + 2) + b;
        }
    });

	$.fn.outerFind = function(selector){
        return this.find(selector).addBack(selector);
    };

    $.isMobile = function(type){
        var reg = [];
        var any = {
            blackberry : 'BlackBerry',
            android : 'Android',
            windows : 'IEMobile',
            opera : 'Opera Mini',
            ios : 'iPhone|iPad|iPod'
        };
        type = 'undefined' == $.type(type) ? '*' : type.toLowerCase();
        if ('*' == type) reg = $.map(any, function(v){ return v; });
        else if (type in any) reg.push(any[type]);
        return !!(reg.length && navigator.userAgent.match(new RegExp(reg.join('|'), 'i')));
    };

    (function($,sr){
        // debouncing function from John Hann
        // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
        var debounce = function (func, threshold, execAsap) {
            var timeout;

            return function debounced () {
                var obj = this, args = arguments;
                function delayed () {
                    if (!execAsap) func.apply(obj, args);
                    timeout = null;
                };

                if (timeout) clearTimeout(timeout);
                else if (execAsap) func.apply(obj, args);

                timeout = setTimeout(delayed, threshold || 100);
            };
        }
        // smartresize 
        jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

    })(jQuery,'smartresize');


    $(function(){
        //mouseover nos menus com submenus
        $(".dropdown").hover(
            function(){ $(this).addClass('open') },
            function(){ $(this).removeClass('open') }
        );

        hasHash = window.location.hash;
        if(hasHash){
            $('html, body').stop().animate({
                 scrollTop: $(hasHash).offset().top - 60
            }, 500,'easeInOutCubic');
        }
        
    });


    $(document).outerFind('.mbr-hamburger:not(.mbr-added)').each(function(){
        $(this).addClass('mbr-added')
            .click(function(){

                //aparece e desaparece com o menu
                $('.menuSecundario').toggleClass('none');

                if($('nav.navbar').hasClass('shrink')){
                    $('nav.navbar-default').toggleClass('navgreen');
                }
                $(this)
                    .toggleClass('mbr-hamburger--open')
                    .parents('.mbr-navbar')
                    .toggleClass('mbr-navbar--open')
                    .removeClass('mbr-navbar--short');
            }).parents('.mbr-navbar').find('a:not(.mbr-hamburger)').click(function(){
                $('.mbr-hamburger--open').click();
            });
    });

    $('ul.dropdown-menu a').bind('click',function(event){
        var $anchor = $(this);
        var page = $(this).attr('data-page');
        var url = $($anchor).attr('href');
        var curUrl = window.location.pathname.replace('.php','');
        
        // curUrl = curUrl.substr(1); //local
        
        //remoto
        curUrl = curUrl.split('/');
        curUrl = curUrl[2]; 

        console.log(curUrl);
        if(curUrl === page ){
            $('html, body').stop().animate({
                 scrollTop: $($anchor.attr('href')).offset().top - 80
            }, 1000,'easeInOutCubic');
        }else{
            var gohref = page + url;
            window.location.href = gohref;
        }
        event.preventDefault();
    });


    $('.sliderDown').on('click',function (e) {
        var $anchor = $(this);
        $('html, body').stop().animate({
             scrollTop: $($anchor.attr('href')).offset().top - 60
        }, 700,'easeInOutCubic');
        e.preventDefault();
    });



    $('.backtotop').on('click',function (e) {
        console.log('back to top');
        $("html, body").animate({scrollTop: 0}, 1000, 'easeInOutCubic');
        e.preventDefault();
    });



    $( ".linkBoo" ).hover(
        function() {
            $(this).find('h3').addClass('verde');
        },function() {
            $(this).find('h3').removeClass('verde');
        }
    );


    $( ".booMenu ul li a" ).hover(
        function() {
            console.log('mouseover');
            // $(this).find('.seta').addClass('hideseta');
            $(this).next().addClass('showarrow');
        },function() {
            console.log('mouseout');
            // $(this).find('.seta').removeClass('hideseta');
            $(this).next().removeClass('showarrow');
        }
    );

	// $('#formContato').validator().on('submit', function (e) {
	//     if (e.isDefaultPrevented()) {
	//         console.log('faltando campos 2');
	//     } else {
	    	
	//     	console.log('enviar form 2');	
	    	
	//     	$('#submitButton').fadeOut('fast', function(){
	// 	    	$('.respostaForm').fadeIn(600);	    		
	//     	});

	//         e.preventDefault();
	//         $.ajax({
	// 			url: "_form_contato.php",
	// 			type: "post",
	// 			data: $("#formContato").serialize(),
	// 			success: function(data) {
	// 				if(data=='enviado'){
	// 					resposta();
	// 				}
	// 			}
	// 		});
	//     }
	// });

    // $('.modal').on('shown.bs.modal', function (e) { 
    //     $(this).find('form[data-toggle=validator]').validator('destroy'); 
    //     $(this).find('form[data-toggle=validator]').validator();
    // });


    $('#btnModal').bind('click',function(event){
        console.log('show modal applied');
        $('#myModal').modal();
        event.preventDefault();
    });


	$('.logo').on('click',function (e) {
		window.location.href = 'index.php';
	});

    $(window).scroll(function(){
        if($(document).scrollTop()>50) {
            $('nav.navbar-default').addClass('shrink');
            $('nav.navbar-default').addClass('navgreen');
        } else{
            $('nav.navbar-default').removeClass('shrink');
            $('nav.navbar-default').removeClass('navgreen');
        }

        if($(document).scrollTop()>800) {
            $('.backtotop').addClass('show');
        }else{
            $('.backtotop').removeClass('show');
        }

    });

})(jQuery);