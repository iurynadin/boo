<?php get_header(); ?>


<div class="slider">

	<!-- <img src="<?php bloginfo('template_url'); ?>/imgs/ui/slider/btn-right.svg" alt=""> -->
	
	<div id="headerCarrousel" class="carousel slide" data-interval="false" data-wrap="true" data-ride="carousel" data-keyboard="true">

		<div class="carousel-inner" role="listbox">

			<div class="item item1-anunciantes" id="0">
				<a href="anunciantes">
					
				<?php include '_slide_anunciantes.php'; ?>
				</a>
			</div>

			<div class="item item1 active" id="1">
				<?php include "_slide_solucoes.php"; ?>
			</div>

			<div class="item item1-negocios" id="2">
				<a href="negocios">
					
				<?php include '_slide_negocios.php'; ?>
				</a>
			</div>
			
		</div>
		<?php include '_slide_controls.php'; ?>


	</div>
	
	<a href="#plataforma" class="sliderDown">down</a>

</div> 


<section class="plataforma" id="plataforma" >
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/grafico01.svg" class="img-responsive grafico01 hidden-md hidden-lg" width="422" alt="">

				<h2>Plataforma Gimbal</h2>
				<p class="centerTablet">Para maximizar o valor da indústria de aparelhos celulares é necessário um conjunto integral de soluções baseadas em . Se você está interessado em criar iniciativas de marketing baseadas em localização, encontrar novas formas de entender audiências em locais físicos específicos ou se está interessado em monetizar seu público-alvo, a Gimbal foi criada especificamente para ser uma plataforma poderosa e escalável para você. <br><br>

				O console de gerenciamento da Gimbal (GIMBAL MANAGER) baseado em nuvem torna simples, rápido e conveniente criar, gerenciar e analisar suas campanhas para celular e infra-estrutura de localização. Se você está usando os melhores “beacons” ou “geofences” da indústria, fornecemos ferramentas para executar com eficiência programas baseados em proximidade e localização. <br><br>

				A plataforma Gimbal ajuda você a otimizar sua estratégia móvel fornecendo as ferramentas necessárias para projetar e entregar experiências únicas e relevantes em macro e micro-localizações.
				</p>
			</div>

			<div class="col-md-6 hidden-xs hidden-sm">
				<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/grafico01.svg" class="img-responsive grafico01" width="422" alt="">
			</div>
				
		</div>
	</div>
</section>


<section class="geofences" id="geofences">
	<div class="container">
		<div class="row">	
			<div class="col-xs-12">
				<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/grafico02.svg" class="img-responsive grafico02 hidden-md hidden-lg" width="197" alt="">
				<h2>Geofênces de última geração</h2>
			</div>

			<div class="col-md-6">
				
				<p class="centerTablet">A Gimbal oferece as soluções de “geofences” mais precisas e eficientes disponíveis no mercado em termos de bateria,  escaláveis em grande escala e de fácil gerenciamento. <br><br>

				As “geofences” da Gimbal são construídas de forma única para permitir que as empresas adotem uma estratégia móvel de macro-localização escalável. Com a capacidade de compreender e envolver um usuário em áreas superiores a 50 metros, uma“geofence” da Gimbal é um perímetro virtual que você pode criar em torno de qualquer área circular ou não circular. Isso torna a solução perfeita para entender a jornada do cliente antes, durante e após visitarem seus locais físicos determinados. <br><br>

				Ao contrário de outras soluções, o nosso “geofencing” do lado do servidor permite 
				que você envolva até usuários inativos que tenham seu aplicativo fechado.
				</p>
			</div>
			<div class="hidden-xs hidden-sm col-md-6">
				<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/grafico02.svg" class="img-responsive grafico02" width="197" alt="">
			</div>
		</div>

	</div>

	<div class="geofencesIcons">

		<div class="container">
			
			<div class="row rowGeofencesIcons"> 
				<div class="col-sm-6 col-md-6 col-lg-3 colGeofenceFlexivel">
					<h3>Flexível</h3>
					<p>Nossos “geofences” de macro-localização podem criar limites radiais (circular) ou poligonais (não-circulares), significando que você pode ser mais exato e preciso. Você também pode projetar seus “geofences” para terem qualquer tamanho acima de 50 metros.</p> 
				</div>

				<div class="col-sm-6 col-md-6 col-lg-3 colGeofenceEscalavel">
					<h3>Escalável</h3>
					<p>Você quer definir e monitorar todos os seus locais com “geofences”? As “geofences” da Gimbal são projetadas para exclusivamente acomodar um número ilimitado de “geofences” em todo o mundo. </p></div>

				<div class="clearfix visible-sm-block visible-md-block"></div>

				<div class="col-sm-6 col-md-6 col-lg-3 colGeofenceAlcance">
					<h3>Alcance Otimizado</h3>
					<p>As “geofences” da Gimbal podem suportar os modos passivo (background) e ativo (primeiro plano), o que significa que você pode se envolver e entender os seus usuários móveis, que tenham seus telefones no bolso ou que estejam usando ativamente o dispositivo.</p> 
				</div>

				<div class="col-sm-6 col-md-6 col-lg-3 colGeofenceGerenciamento">
					<h3>Fácil Gerenciamento</h3>
					<p>No Gimbal Manager, você pode facilmente definir, gerenciar e analisar suas “geofences”. Adicionar ou remover localizações de “geofence”? Tornamos fácil para você alterar essas “fronteiras” rapidamente, sem a necessidade de atualizar seu aplicativo móvel ou enviá-lo novamente para a loja de aplicativos (app stores) para aprovação.</p>
				</div>

				<div class="col-xs-12"><p class="text-center">Saiba como nossas “geofences” de macro-localização podem ajudar você a entender facilmente toda a jornada de seus clientes.</p></div>

			</div>
		</div>

		
	</div>

</section>


<section class="beacons" id="beacons">
	<div class="container">
		<div class="row">	
			<div class="col-xs-12">
				<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/grafico03.svg" class="img-responsive grafico03 hidden-md hidden-lg" width="187" alt="">
				<h2>Beacons de micro-localização</h2>
			</div>

			<div class="col-md-6">
				
				<p class="centerTablet">Os “beacons” da Gimbal usam “Bluetooth Low Energy” (BLE) para permitir que você compreenda e envolva seu cliente através dos dispositivos móveis com alcance de centímetros até 50 metros. Criamos “beacons” com a máxima flexibilidade, escalabilidade e segurança para que você possa ter certeza de que está implementando a melhor e mais eficaz solução de micro-localização disponível.<br><br>

				Capaz de ser usado em ambientes fechados ou ao ar livre, nossos “beacons” tem vários formatos e suportam várias configurações (Apple iBeacon ™, Google Eddystone ™ e Gimbal Secure Mode).<br><br>
				</p>


			</div>

			<div class="col-md-6 hidden-xs hidden-sm">
				<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/grafico03.svg" class="img-responsive grafico03" width="187" alt="">
			</div>

			<div class="clear"></div>


			<div class="col-xs-12 col-md-8 col-md-offset-2 paddingtopbottom">
				
				<h4 class="text-center">USADO PELOS PRINCIPAIS ESPAÇOS DE ENTRETENIMENTO, ESPORTES, FEIRAS E VAREJO.</h4>
				<br>

				<div class="row">
					<div class="col-xs-3 col-md-3 iconBeacon">
						<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/icon_beacon03a.svg" width="90" alt="" class="img-responsive">
					</div>
					<div class="col-xs-3 col-md-3 iconBeacon">
						<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/icon_beacon02.svg" width="75" alt="" class="img-responsive">
					</div>
					<div class="col-xs-3 col-md-3 iconBeacon">
						<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/icon_beacon03.svg" width="80" alt="" class="img-responsive">
					</div>
					<div class="col-xs-3 col-md-3 iconBeacon">
						<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/icon_beacon04.svg" width="75" alt="" class="img-responsive">
					</div>
				</div>

				<br><br>
			</div>

			
		</div>
	</div>


	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<h3 class="ico-seguranca">Seguranca Prorietária</h3>
				<p>Transmissões seguras garantem o mais alto nível de privacidade juntamente com experiências gerenciadas aos consumidores, ao mesmo tempo em que oferecem aos proprietários dos “beacons” a oportunidade de "possuir digitalmente" seu inventário de “beacons”. Isso permite que esses “proprietários” gerenciem experiências  móveis alocando com segurança o acesso aos aplicativos móveis e parceiros que eles autorizem. Assim, seus parceiros são capazes de fornecer uma experiência única ao protegê-los de “showrooming” ou “conquesting”.</p> </div>

			<div class="col-xs-12 col-sm-6">
				<h3 style="padding-top:6px; padding-bottom:6px;">Melhores antenas significam melhores dados</h3>
				<p>Graças aos seus inovadores e inéditos desenhos de antenas, omni direcionais, os “beacons” Gimbal têm uma claridade de sinal superior. Isso significa que é mais fácil configurar seus “beacons” e começar a alavancar sua plataforma, enquanto requer menos tempo e mão-de-obra para habilitar o seu “hardware”. Isso proporciona mais possibilidades de uso possível, experiências de usuário previsíveis / repetíveis, maior precisão para seus engajamentos e melhores dados de suas campanhas.</p>
			</div>

			<div class="clearfix visible-sm-block visible-md-block visible-lg-block"></div>

			<div class="col-xs-12 col-sm-6">
				<h3>Maior eficiência para uma manutenção reduzida</h3>
				<p>Com os transmissores Bluetooth® Smart (Bluetooth Low Energy) e engenharia de alto nível, os nossos ” beacons” mais modernos duram 18 meses operando na mais alta taxa de transmissão e duram ainda mais tempo em outras configurações, com apenas quatro pilhas AA. Eles também transmitem a cada 100ms continuamente e fornecem dados de temperatura e nível da bateria para ajudar a simplificar a manutenções e operações.</p> 
			</div>

			<div class="col-xs-12 col-sm-6">
				<h3>Gestão simplificada e desenvolvimento</h3>
				<p>O Gimbal Manager facilita o gerenciamento de todos os “beacons” em sua rede, mesmo os de outros fabricantes. Ele também permite a fácil atualização de todos os “beacons” Gimbal. Os “beacons” Gimbal podem ser rapidamente configurados para transmitir em vários modos, incluindo o modo iBeacon. Isso garante uma plataforma personalizada que pode transmitir em dispositivos iOS e Android. Dependendo de suas necessidades, o proprietário "Gimbal Mode"fornece camadas adicionais de segurança para um controle mais profundo de sua localização e rede de proximidade, enquanto também simplifica a implantação e sua gestão. Isso reduz os custos operacionais e torna os “beacons” da Gimbal os mais fáceis de usar do setor.</p> 
			</div>

			<div class="clearfix visible-sm-block visible-md-block visible-lg-block"></div>

			<div class="col-xs-12 col-sm-6">
				<h3>Optimizado para uso interno e externo</h3>
				<p>Com a flexibilidade em mente, nossos últimos “beacons” são projetados para as especificações NEMA 3 e foram comprovados pelo desempenho excepcional, tanto em ambientes fechados como também ao ar livre. Isso significa que os elementos não são mais uma barreira para envolver seus clientes com as informações mais relevantes e oportunas.</p> </div>

			<div class="col-xs-12 col-sm-6">
				<h3>"Firmware" flexível</h3>  <!-- class="ico-firmware" -->
				<div class="ico-firmware">
					<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/icon-firmware.svg" width="150" alt="">
				</div>
				<p>Os fabricantes interessados em evoluir suas ofertas para se adequar às tendências da “Internet-das-Coisas (IoT)”, podem agora licenciar o firmware Bluetooth Gimbal para  construir novas funcionalidades através dos “beacons” em produtos anteriormente  inexplorados, como lâmpadas, pontos de acesso WiFi,  automação de casas, cidades-inteligentes, ou qualquer coisa que necessite de uma descoberta digital segura.  Esses dispositivos podem funcionar como “beacons”, comunicando-se com dispositivos móveis inteligentes e tornando-se parte da rede de “beacons” Gimbal. O firmware proprietário da Gimbal é capaz de rodar em plataformas embutidas existentes,  incluindo Windows®, Linux®, Android ™, Intel x86 e sistemas proprietários, entre outros. Através desta solução, a Gimbal pode licenciar a capacidade de transmitir e detectar “beacons”.</p> </div>
			
		</div>
	</div>
</section>


<section class="experiencias" id="experiencias">
		<div class="bgCinzaInParallax">
			<div class="container">
				<div class="row">	

					<div class="col-md-6">

						<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/grafico04.svg" class="img-responsive grafico04 hidden-md hidden-lg" width="266" alt="">

						<h2>Experiências Gimbal</h2>
						
						<p class="centerTablet">“Gimbal Experiences” é uma suíte criativa simples para os profissionais de marketing criarem experiências ricas em seus aplicativos móveis que são entregues aos usuários nas “geofences” de macro-localização e nos “beacons” de proximidade em que estão interessados.</p>

						<h4 class="centerTablet">USADO PELOS PRINCIPAIS ESPAÇOS DE ENTRETENIMENTO, ESPORTES, FEIRAS E VAREJO.</h4>

						<p class="centerTablet">
						Sem necessidade de codificação, arraste e solte para fácil utilização e integração perfeita em ferramentas existentes. Segmente suas experiências móveis por públicos-alvo e locais para criar jornadas de usuário personalizadas e relevantes<br><br>

						Obtenha novos conhecimentos e análises sobre o desempenho de suas campanhas de marketing nos diferentes locais que você monitora. 
						</p>

					</div>
					<div class="col-md-6 hidden-xs hidden-sm">
						<img src="<?php bloginfo('template_url'); ?>/imgs/solucoes/grafico04.svg" class="img-responsive grafico04" width="266" alt="">
					</div>
				</div>
			</div>
		</div>


		<div class="experienciasIcons" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/imgs/backgrounds/parallax_solucoes.jpg" > 

			<div class="container">
				
				<div class="row rowExperienciasIcons">
					<div class="col-sm-6 col-md-3 col-lg-3 iconExp01">
						<h5>INCENTIVE O USO DE APLICATIVOS MÓVEIS</h5>
						<p>Aumente Downloads, Uso e Engajamento Móvel </p> 
					</div>

					<div class="col-sm-6 col-md-3 col-lg-3 iconExp02">
						<h5>MAXIMIZE VENDAS & LUCROS</h5>
						<p>“Cross-Sell”, Ofertas Especiais, Queima de Estoque</p></div>

					<div class="clearfix visible-sm-block"></div>

					<div class="col-sm-6 col-md-3 col-lg-3 iconExp03">
						<h5>PROMOVA COMPROMETIMENTO E FIDELIDADE</h5>
						<p>Incentive usuários, desbloqueie conteúdo, resgate prêmios</p> 
					</div>

					<div class="col-sm-6 col-md-3 col-lg-3 iconExp04">
						<h5>TESTE & ANÁLISE</h5>
						<p>Compreenda o comportamento do consumidor no mundo real</p>
					</div>

				</div>
			</div>
		</div>

	</section>






<?php get_footer(); ?>