<?php get_header(); ?>

<div class="slider">
		
	<div id="headerCarrousel" class="carousel slide" data-interval="false" data-wrap="true" data-ride="carousel" data-keyboard="true">

		<div class="carousel-inner" role="listbox">

			<div class="item item1-home" id="0">
				<a href="quemsomos">
					<?php include '_slide_quemsomos.php'; ?>
				</a>
			</div>

			<div class="item item1-sdk active" id="1">
				<?php include '_slide_sdk.php'; ?>
			</div>

			<div class="item item1-publicidademovel" id="2">
				<a href="publicidademovel">
					<?php include '_slide_publicidademovel.php'; ?>
				</a>
			</div>
			
		</div>

		<?php include '_slide_controls.php'; ?>


	</div>

	<a href="#valordados" class="sliderDown">down</a>

</div> 
<!-- slider -->


<?php include "_menuinterna_boo.php"; ?>


<section class="valorDados" id="valordados">
	<div class="container">

		<div class="row">	
			<div class="col-xs-12">

				<br><br>
				<h6 class="text-center">INTELIGÊNCIA DE GEOLOCALIZAÇÃO PARA MARCAS E AGÊNCIAS QUE QUEIRAM ENCONTRAR<br>OS MOMENTOS MAIS ADEQUADOS PARA COMUNICAREM-SE COM OS CIDADÃOS.</h6>
				<br><br>

				<h2 class="text-center">O valor dos dados está em compreender o seu público</h2>

				<img src="<?php bloginfo('template_url'); ?>/imgs/sdk/sdkimg01.jpg" height="312" width="949" class="img-responsive ImgApp2" alt="">
				<br>
			</div>
		</div>

			
		<div class="row rowSdk"> 

			<div class="col-md-4 col-lg-4 colSdkIcon1">
				<p>Mais de 70% das decisões de compra são feitas perto do ponto de venda. </p>
			</div>

			<div class="col-md-4 col-lg-4 colSdkIcon2">
				<p>As marcas e suas agências querem identificar pessoas que correspondem seu perfil de compra no momento em que elas podem influenciar suas decisões de compra. </p>
			</div>

			<div class="col-md-4 col-lg-4 colSdkIcon3">
				<p>O Gimbal SDK e suas ferramentas possibilitam aos anunciantes a capacidade de direcionar e entregar comunicações com base em momentos que um cliente tem mais propensão a comprar.</p> 
			</div>

		</div>

		<div class="row">
			<div class="col-xs-12">
				<h4 class="text-center">em seguida, dando-lhes a informação que eles necessitam</h4>
				<br>

				<h2 class="text-center">Entender o seu público</h2>
			</div>

		</div>


		<div class="row">
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img src="<?php bloginfo('template_url'); ?>/imgs/sdk/sdk_print01.jpg" class="postImg img-responsive" alt="">
				</div>
			</div>

			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img src="<?php bloginfo('template_url'); ?>/imgs/sdk/sdk_print02.jpg" class="postImg img-responsive" alt="">
				</div>
			</div>

			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<img src="<?php bloginfo('template_url'); ?>/imgs/sdk/sdk_print03.jpg" class="postImg img-responsive" alt="">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<h4 class="text-center">Além de entregar as comunicações com base na localização, a tecnologia Gimbal:</h4>
			</div>
		</div>
		

	</div>

</section>


<section class="others">
	<div class="container">

		<div class="row rowLocalizacao">
			<div class="col-md-3">
				<img src="<?php bloginfo('template_url'); ?>/imgs/sdk/icon-solucao.svg" width="158" class="iconSolucao01" alt="">
			</div>

			<div class="col-md-9">
				<ul class="ulsdk">
					<li>Mede a adoção e a atividade de aplicativos, e aprende as tendências dos usuários; </li>
					<li>Cria segmentos de usuários(clusters) e como usá-los nas comunicações (como locais mais frequentados)</li>
					<li>Analisa o tráfego de transeuntes, tempo de permanência nos locais e os padrões de visitantes que retornam.</li>
					<li>Sabe quais campanhas tiveram mais engajamento e visitas.</li>
				</ul>
			</div>
		</div>

	</div>
</section>



<section class="gimbaeboo">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2>SDK Gimbal e BOO! <span>são uma solução única para a monetização de dados de geolocalização</span></h2>
			</div>
		</div>

		<div class="row">
			
			<div class="grafico">

				<div class="quad1">
					<p>A maioria dos SDKs de publicidade que monitoram função de localização apenas funcionam no primeiro plano. Isto exige que o aplicativo permaneça aberto. Isso limita o número de pontos de captura de dados. <br><br> O SDK do Gimbal pode coletar dados de localização em primeiro plano, background e mesmo quando o aplicativo não estiver em execução.
					</p>
				</div>

				<div class="quad2">
					<p>O SDK do Gimbal gera mais pontos de dados de localização do que SDKs que usam apenas beacons. Isso nos permite reunir e monetizar mais registros de dados.<br><br>O SDK do Gimbal monitora geo-fences (cercas virtuais) com formas radiais e poligonais com alta precisão. Tudo isso pode ser usado para a atribuição de anúncios no mundo físico.</p>
				</div>

				<img src="<?php bloginfo('template_url'); ?>/imgs/sdk/gimbaleboo3.svg" class="img-responsive gimbalbooImg hidden-md hidden-lg" alt="">

				<div class="quad3">
					<p>O Gimbal SDK é instalado em mais de 300 milhões de dispositivos. Ele passou nos testes de segurança e estabilidade empresarial em grandes instituições financeiras. É um produto maduro, com mais de US$40MM de P & D dentro Qualcomm.
					</p> 
				</div>
				<div class="quad4">
					<p>A rede Gimbal é um dos maiores do mundo. O Gimbal SDK também suporta padrões abertos como iBeacon e Eddystone e Google Web Física.</p>
				</div>
			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>