<article class="post">
	<div class="row">
		<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
			<span class="data"><?php the_time('d.m.y'); ?></span>
			<a href="<?php the_permalink(); ?>" class="postTitle"><?php the_title(); ?></a>
			<!-- <span class="postTitle-title"><?php the_title(); ?></span> -->
			<div class="publisher">Publicado por <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><i><?php the_author(); ?></i></a></div>

			<?php if(the_post_thumbnail()){ ?>
			<img src="<?php the_post_thumbnail_url('small-thumbnail'); ?>" class="postImg img-responsive" alt="">
				</br>

			<?php } ?>

			<?php the_content(); ?>

			<hr>

			<div class="holderCateg">
				<?php 
					$categories = get_the_category(); 
					$output = '';

					if($categories){
						foreach ($categories as $category) {
							$output .=  '<a href="'. get_category_link($category->term_id) .'" class="categoria">' . $category->cat_name . '</a>';
						}

						echo $output;
					}
				 ?>				
			</div>

		</div>
	</div>
</article>