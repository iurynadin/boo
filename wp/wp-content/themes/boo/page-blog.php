<?php get_header(); ?>

<section class="blogHeader" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/imgs/backgrounds/blog.jpg">
	<div class="container">
		<div class="row">
			<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
		</div>
	</div>
</section>


<div class="container">

	<div class="row">
		<div class="col-xs-12"><h2>Blog</h2></div>
	</div>

	<?php 


		$ourCurrentPage = get_query_var('paged');

		$allPosts = new WP_Query(array(
			'posts_per_page' => 3,
			'paged' => $ourCurrentPage
		));

		if($allPosts->have_posts()) : 

			while($allPosts->have_posts()) : $allPosts->the_post();

				include '_part_post.php';

			endwhile;

			?>


			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
					<div class="pagination">

						<?php echo paginate_links(array(
							'total' => $allPosts->max_num_pages,
							'prev_text' => __('<'),
							'next_text' => __('>'),
							'type' => 'list'
						)); ?>

					</div>
				</div>
			</div>

		<?php

		else :

		endif;

	?>

</div>

<?php get_footer(); ?>