<?php get_header(); ?>

<div class="slider">
		
	<div id="headerCarrousel" class="carousel slide" data-interval="false" data-wrap="true" data-ride="carousel" data-keyboard="true">

		<div class="carousel-inner" role="listbox">

			<div class="item item1-sdk" id="0">
				<a href="sdk">
					<?php include '_slide_sdk.php'; ?>
				</a>
			</div>

			<div class="item item1-publicidademovel2 active" id="1">
				<?php include '_slide_publicidademovel.php'; ?>
			</div>
			
			<div class="item item1-home" id="2">
				<a href="quemsomos">
					<?php include '_slide_quemsomos.php'; ?>
				</a>
			</div>
		</div>

		<?php include '_slide_controls.php'; ?>

	</div>

	<a href="#mobilemajority" class="sliderDown">down</a>

</div> 
<!-- slider -->

<?php include "_menuinterna_boo.php"; ?>


<section class="mobileMajority" id="mobilemajority" >
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				
				<br><br>
				<h6 class="text-center">UM CONJUNTO ALTAMENTE INTEGRADO DE SOLUÇÕES DE PUBLICIDADE MÓVEL PARA CRIAÇÃO, AMPLIFICAÇÃO DE ALCANCE, MEDIÇÕES E INSIGHTS QUE GARANTEM MAIOR DESEMPENHO DO QUE QUALQUER OUTRA PLATAFORMA DE ANÚNCIOS PARA CELULAR.</h6>
				<br><br>

				<!-- <h2>The Mobile Majority</h2> -->

				<img src="<?php bloginfo('template_url'); ?>/imgs/publicidademovel/logo_publicidademovel.png" width="411" class="img-responsive" alt="">
				
				<img src="<?php bloginfo('template_url'); ?>/imgs/publicidademovel/grafico01.svg" class="img-responsive grafico01 hidden-md hidden-lg" width="400" alt="">
				
				<h4>É uma solução/plataforma completa de mídia mobile totalmente integrada.</h4>

				<div class="holderGrafico hidden-xs hidden-sm">
					<img src="<?php bloginfo('template_url'); ?>/imgs/publicidademovel/grafico01.svg" class="img-responsive" width="380" alt="">
				</div>

				<p>Em outras palavras, desenvolvemos todo o ferramental possível para executar uma campanha com qualidade:</p> 

				<ul class="paragraph">
					<li>Ferramenta de gestão dos Banners</li>
					<li>Além da criação e animação dos mesmos</li>
					<li>Segmentação utilizando dados</li>
					<li>Compra da mídia DSP e por último mensuração dos resultados. </li>
				</ul> 

				<p>
				Além disso, utilizamos o conceito de People Based Audience / People Based Marketing que foi lançado no mercado pelo Facebook em 2014. <br><br>

				Isso significa que combinamos dados PII com não PII (cookies), conseguindo mais precisão e assertividade na segmentação de audiências e isso se reflete em resultados muito acima da média da industria. <br><br>

				Outro fator importante de diferenciação é a velocidade que conseguimos "bidar" a mídia (RTB), a qual é bem mais veloz que a média do mercado e só conseguimos isso pois construimos todo o ferramental para se realizar uma campanha de mídia mobile com sucesso. <br><br>

				Também vale mencionar que por desenvolvermos os criativos, garantimos a entrega em múltiplos devices e sistemas operacionais sem que haja quebra de banner. Enfim, combinando todos estes fatores e mais alguns, conseguimos entregar campanhas com uma performance bastante superior a média de mercado seja qual for o KPI que você estiver medindo. <br><br>

				Em termos comerciais podemos vender mídia direta para agências e anunciantes como também vender nossa ferramenta para os publishers, assim eles potencializam seu inventário e a performance de suas campanhas.
				
				</p>
			</div>
			<!-- <div class="col-md-5">
					<img src="<?php bloginfo('template_url'); ?>/imgs/publicidademovel/grafico01.svg" class="img-responsive grafico01" width="350" alt="">
			</div> -->
		</div>
	</div>
</section>


<section class="valor">

	<div class="valorIcons" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/imgs/backgrounds/solucoes_experiencias.jpg" > 

		<div class="container">

			<div class="row">
				<div class="col-xs-12">
					<h2>VALOR ÚNICO HABILITADO POR INTEGRAÇÃO COMPLETA</h2>
					<p>Temos três vantagens que não podem ser replicadas usando uma abordagem fragmentada e de terceiros.</p>
				</div>
			</div>
			
			<div class="row rowExperienciasIcons">
				<div class="col-md-4 iconValor01">
					<h5>1.CAPACIDADE DE VISUALIZAR ANÚNCIOS</h5>
					<p>Nem todas as impressões são iguais. É por isso que a nossa equipa de Data Science construiu o algoritmo que pode prever se um anúncio será visível - antes de pagar por ele. </p>
				</div>

				<div class="col-md-4 iconValor02">
					<h5>2.ATIVAÇÃO DE DADOS</h5>
					<p>Use nossos miríades de pontos de dados proprietários para sua campanha e entregue o anúncio correto à pessoa certa no ambiente ideal.
					</p>
				</div>

				<div class="col-md-4 iconValor03">
					<h5>3.ENTREGA FINAL</h5>
					<p>O final dos anúncios quebrados. Seu alto impacto criativo é entregue através da nossa tecnologia proprietária que proporciona uma qualidade impecável para qualquer aplicativo ou site.
					</p> 
				</div>

			</div>

			<div class="row">
				<div class="col-xs-12">
					<h4 class="text-center white">INTEGRAMOS TODO O ECOSISTEMA MOBILE</h4>
				</div>
			</div>
		</div>
	</div>

</section>



<?php get_footer(); ?>