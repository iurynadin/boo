<div class="slider">

	<!-- <img src="dist/imgs/ui/slider/btn-right.svg" alt=""> -->
	
	<div id="headerCarrousel" class="carousel slide" data-interval="false" data-wrap="true" data-ride="carousel" data-keyboard="true">

		<div class="carousel-inner" role="listbox">

			<div class="item item1 active" id="0">
				
				<div class="holderItems">
					<div class="sliderTitle">Maximize sua estratégia móvel</div>
					<div class="sliderSubtitle">First class mobile experience at macro and micro locations</div>
					<p>Build, manage and analyse your mobiles campaings and location infrastructure</p>
					<div class="container contSlider hidden-xs">
						<div class="row">
							<div class="col-xs-6 col-md-3 iconLocalizacao">
								<span>Localização</span>
							</div>
							<div class="col-xs-6 col-md-3 iconProximidade">
								<span><b>Proximidade</b></span>
							</div>
							<div class="col-xs-6 col-md-3 iconDatasolutions">
								<span>Data Solutions</span>
							</div>
							<div class="col-xs-6 col-md-3 iconAnalytics">
								<span><b>Analytics</b></span>
							</div>
						</div>
					</div>
				</div>

			</div>
			
			<div class="item item2" id="1">
				<div class="holderItems">
					<div class="sliderTitle">Título</div>
					<p>Subtitulo</p>
				</div>
			</div>


			<!-- <div class="item item3" id="2">
				<div class="holderItems">
					<div class="sliderTitle">Título</div>
					<p>Subtitulo</p>
				</div>
			</div>

			<div class="item item4" id="3">
				<div class="holderItems">
					<div class="sliderTitle">Título</div>
					<p>Subtitulo</p>
				</div>
			</div>	 -->	
		</div>


		<ol class="carousel-indicators">
			<li data-target="#headerCarrousel" data-slide-to="0" class="active"></li>
			<li data-target="#headerCarrousel" data-slide-to="1"></li>
			<!-- <li data-target="#headerCarrousel" data-slide-to="2"></li>
			<li data-target="#headerCarrousel" data-slide-to="3"></li> -->
		</ol>


		<a class="left carousel-control hidden-xs" href="#headerCarrousel" role="button" data-slide="prev"></a>
		<a class="right carousel-control hidden-xs" href="#headerCarrousel" role="button" data-slide="next"></a>

	</div>

</div> 
<!-- slider -->